<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {
  /**
   * Authenticates a user.
   */
  private $_id;

  public function authenticate() {
    
    //Authenticate with User Model
    
    //Create hasher to use to check password
    $hasher = new PasswordHash(8,FALSE);
    
    //Attempt to find the user's record
    $user_record = User::model()->findByAttributes(array('username'=>$this->username));
    
    if ($user_record == null){
      $this->errorCode=self::ERROR_USERNAME_INVALID;
    } elseif (!$hasher->CheckPassword($this->password,$user_record->password)) {
      $this->errorCode=self::ERROR_PASSWORD_INVALID;
    } else {
      //Login success
      $this->_id = $user_record->user_id;
      $this->setState('id',$user_record->user_id); //Give CWebUser the id of the user
      $this->setState('name',$user_record->username); //Give CWebUser the id of the user
      $this->setState('email',$user_record->email); //Give CWebUser the id of the user
      $this->errorCode=self::ERROR_NONE;
    }

    return !$this->errorCode;
  }

  public function getId(){
    return $this->_id;
  }

}
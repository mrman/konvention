<?php
/*Karma widget
 *Expecting:
 *@var $karma Karma object to use to calculate
 */

   $karma = $model->getKarma();
$difference = ($karma->likes - $karma->dislikes);
?>

<div class="input-prepend input-append" id="widget-karma">
   <button class="btn btn-success" type="button"><i class="icon-thumbs-up icon-white"></i> </button>
  <input type="text" class="input-mini" id="differential" disabled="disabled" value="<?php echo $difference ?>"/>
  <button class="btn btn-danger" type="button"><i class="icon-thumbs-down icon-white"></i> </button>
</div>

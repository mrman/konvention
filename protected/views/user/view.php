<?php
/* @var $this UserController */
/* @var $model User */

if(Yii::app()->user->name === 'vados') {
  $this->breadcrumbs=array(
			   'Users'=>array('index'),
			   $model->user_id,
			   );

  //Note -- use $model to get at stuff related to the user ON PAGE.

  $this->menu=array(
		    array('label'=>'List User', 'url'=>array('index')),
		    array('label'=>'Create User', 'url'=>array('create')),
		    array('label'=>'Update User', 'url'=>array('update', 'id'=>$model->user_id)),
		    array('label'=>'Delete User', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->user_id),'confirm'=>'Are you sure you want to delete this item?')),
		    array('label'=>'Manage User', 'url'=>array('vados')),
		    );
} else {
  //User Breadcrumb
  $this->breadcrumbs=array(
			   'Users',
			   "User Profile (" . $model->username . ")",);
}

//Look up the UserInfo object associated with this user
$userinfo = UserInfo::model()->findByPk($model->user_id);

//Clean the user ids for the user
$fb_id = htmlspecialchars($userinfo->facebook_id);
$gplus_id = htmlspecialchars($userinfo->googleplus_id);
$twit_id = htmlspecialchars($userinfo->twitter_id);
$git_id = htmlspecialchars($userinfo->github_id);
$tumb_id = htmlspecialchars($userinfo->tumblr_id);

?>

<?php if (Yii::app()->user->id === $model->user_id): ?>
<a href="<?php echo Yii::app()->createUrl('user/update',array('id'=>Yii::app()->user->id)) ?>" class="btn btn-info btn-medium pull-right" id="btnUpdateUserProfile">Update your profile</a>
<?php endif;?>
<h1>Reach out to [<?php echo $model->username; ?>]:</h1>

<div class="well slidedown-item header-theme slim-well">
  <i class="fc-webicon facebook large vert-align-mid"></i>
  <span class="large"><?php echo (($userinfo->facebook_id == NULL) ? 'None Specified...' : "<a href=\"http://www.facebook.com/$fb_id\">$fb_id</a>")?></span>
</div>

<div class="well slidedown-item header-theme slim-well">
  <i class="fc-webicon googleplus large vert-align-mid"></i>
  <span class="large"><?php echo (($userinfo->googleplus_id == NULL) ? 'None Specified...' : "<a href=\"http://www.google.com/profiles/$gplus_id\">$gplus_id</a>")?></span>
</div>

<div class="well slidedown-item header-theme slim-well">
  <i class="fc-webicon twitter large vert-align-mid"></i>
  <span class="large"><?php echo (($userinfo->twitter_id == NULL) ? 'None Specified...' : "<a href=\"https://www.twitter.com/$twit_id\">$twit_id</a>")?></span>
</div>

<div class="well slidedown-item header-theme slim-well">
  <i class="fc-webicon github large vert-align-mid"></i>
  <span class="large"><?php echo (($userinfo->github_id == NULL) ? 'None Specified...' : "<a href=\"https://www.github.com/$git_id\">$git_id</a>")?></span>
</div>

<div class="well slidedown-item header-theme slim-well">
  <i class="fc-webicon tumblr large vert-align-mid"></i>
  <span class="large"><?php echo (($userinfo->tumblr_id == NULL) ? 'None Specified...' : "<a href=\"$tumb_id.tumblr.com\">$tumb_id</a>")?></span>
</div>

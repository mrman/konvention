<?php
/* @var $this UserController */
/* @var $model User */
/* @var $userinfo UserInfo related to given User */
/* @var $form CActiveForm */


?>

<form id="frmUser" method="post">
    <p class="note">Fields with <span class="required">*</span> are required.</p>

   <h3>Personal</h3>

   <?php if ($model->isNewRecord):?>
   <label for="frmUser-field-username">Username<sup class="required-field">*</sup></label>
   <input size="60" maxlength="128" name="User[username]" placeholder="Username" id="frmUser-field-username" type="text">
   <?php endif ?>

   <label for="frmUser-field-password">Password<sup class="required-field">*</sup></label>
    <input size="60" maxlength="128" name="User[password]" id="frmUser-field-password" type="password" value="<?php echo $model->password; ?>">

   <label for="frmUser-field-email">Email<sup class="required-field">*</sup></label>
    <input size="60" maxlength="128" name="User[email]" id="frmUser-field-email" type="text" value="<?php echo $model->email; ?>">

   <h3>Social (optional)</h3>
   <label for="frmUser-field-facebook_id"><i class="fc-webicon facebook medium vert-align-mid"></i> Facebook</label>
   <input size="60" maxlength="128" name="UserInfo[facebook_id]" placeholder="Facebook username" id="frmUser-field-facebook_id" type="text" value="<?php echo $userinfo->facebook_id; ?>">

   <label for="frmUser-field-googleplus_id"><i class="fc-webicon googleplus medium vert-align-mid"></i> Google+</label>
   <input size="60" maxlength="128" name="UserInfo[googleplus_id]" placeholder="Google+ username" id="frmUser-field-googleplus_id" type="text" value="<?php echo $userinfo->googleplus_id; ?>">

   <label for="frmUser-field-twitter_id"><i class="fc-webicon twitter medium vert-align-mid"></i> Twitter</label>
   <input size="60" maxlength="128" name="UserInfo[twitter_id]" placeholder="Twitter username" id="frmUser-field-twitter_id" type="text" value="<?php echo $userinfo->twitter_id; ?>">

   <label for="frmUser-field-github_id"><i class="fc-webicon github medium vert-align-mid"></i> Github</label>
   <input size="60" maxlength="128" name="UserInfo[github_id]" placeholder="Github username" id="frmUser-field-github_id" type="text" value="<?php echo $userinfo->github_id; ?>">

   <label for="frmUser-field-tumblr_id"><i class="fc-webicon tumblr medium vert-align-mid"></i> Tumblr</label>
   <input size="60" maxlength="128" name="UserInfo[tumblr_id]" placeholder="Tumblr username" id="frmUser-field-tumblr_id" type="text" value="<?php echo $userinfo->tumblr_id; ?>">

<br/>
<br/>

    <button type="submit" class="btn btn-success btn-large"><?php echo (($model->isNewRecord) ? 'Sign Up' : 'Update'); ?></button>
</form>


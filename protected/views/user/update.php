<?php
/* @var $this UserController */
/* @var $model User */
/* @var $userinfo UserInfo of the User */

if(Yii::app()->user->name === 'vados') {
  //Admin Breadcrumbs for ease of navigation
  $this->breadcrumbs=array(
			   'Users'=>array('index'),
			   $model->user_id=>array('view/' . $model->user_id),
			   'Update',
			   );

  $this->menu=array(
		    array('label'=>'List User', 'url'=>array('index')),
		    array('label'=>'Create User', 'url'=>array('create')),
		    array('label'=>'View User', 'url'=>array('view', 'id'=>$model->user_id)),
		    array('label'=>'Manage User', 'url'=>array('vados')),
		    );
} else {
  //Display different breadcrumbs for users
  $this->breadcrumbs=array(
			   'Users',
			   'Update(' . $model->username . ')',
			   );
}
?>

<h1>Update User Information</h1>

<p class="large">Update your user information</p>

<?php echo $this->renderPartial('_form', array('model'=>$model,'userinfo'=>$userinfo)); ?>
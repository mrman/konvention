<?php
   /* @var $results Array The results of the search query */

   //Set breadcrumbs
   $this->breadcrumbs=array('Search Results',);
?>

<h2>Results for '<?php echo $_REQUEST['query']?>':</h2>

<?php if (isset($results) && !is_null($results)): ?>
<h3 class="page-section-header">Best Practices:</h3>
<table class="table table-condensed table-bordered" id="search_results">
  <thead>
    <tr>
      <th>Name</th>
      <th>Context</th>
      <th>Description</th>
    </tr>
  </thead>
  <tbody> 
    <?php
       foreach ($results['bestpractices'] as $data)
       $this->renderPartial("/search/_contextListItem", array('data'=>$data));    
    ?>
  </tbody>
</table>

<?php else: ?>
<h2>Invalid Search</h2>
<p>Looks like something went wrong with your search query... please return to <a href="">Konvention</a> and try again.</p>
<?php endif; ?>



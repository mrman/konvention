<tr>
  <td><a href="<?php echo Yii::app()->createUrl("context/" . $data->context->name . "/bestpractice/" . $data->name)?>"><?php echo CHtml::encode($data->name) ?></a></td>
   <td><a href="<?php echo Yii::app()->createUrl("context/view/" . $data->context->name)?>"><?php echo htmlspecialchars($data->context->name); ?></a></td>
  <td class="truncated"><?php echo nl2br(htmlspecialchars($data->description))?></td>
</tr>

<?php
   /* @var $this BestpracticeController */
   /* @var $model BestPractice */

//Lookup context
$context = Context::model()->findByPk($model->context_id);

   $this->breadcrumbs=array(
			    'Context'=>array('context/index'),
			    $context->name=>array('context/view/' . $context->name),
			    'Best Practice',			   
			    );

#Clean some data about to be displayed on the page
$snippet_safe = htmlspecialchars($model->snippet);
$desc_safe = nl2br(htmlspecialchars($model->description));
$ref_url_safe = htmlspecialchars($model->ref_url);

?>


<h1><?php echo $model->name ?></h1>
<?php $this->renderPartial("/karma/_widget", array('model'=>$model,'karma'=>$model->getKarma())); ?>


<br/>
<br/>
<dl class="dl-horizontal">

  <dt>Snippet</dt>
     <dd><pre class="prettyprint" style="padding:.5em;"><?php echo $snippet_safe ?></pre></dd>
  <br/>

  <dt>Description</dt>
     <dd><?php echo $desc_safe ?></dd>
  <br/>

  <dt>Context</dt>
  <dd><a href="<?php echo Yii::app()->createUrl('context/view/' . $context->name) ?>"><?php echo $context->name ?></a></dd>
  <br/>

  <dt>Convention Type</dt>
  <dd><?php echo $model->semanticName() ?></dd>
  <br/>

  <dt>Reference URL</dt>
  <dd><a href="<?php echo $ref_url_safe ?>"><?php echo $ref_url_safe ?></a></dd>  
  <br/>
</dl>

<?php $this->renderPartial("/comment/_list", array('model'=>$model)); ?>

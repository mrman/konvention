<?php
/* @var $this BestpracticeController */

$this->breadcrumbs=array(
	'Best Practice');
?>

<h1>Most Popular Best Practices</h1>

<table data-src="<?php echo Yii::app()->createUrl('bestpractice/list'); ?>" class="table table-condensed table-bordered" id="bp_list">
  <thead>
    <tr>
      <th>Name</th>
      <th>Context</th>
      <th>Description</th>
      <th>Reference URL</th>
      <th>Likes</th>  
    </tr>
  </thead>
  <tbody> 
  </tbody>
</table>
<?php
/* @var $context name of the Context to submit the best practice under (autoselected in context list) */
/* @var $error String to represent an error that occured */

$this->breadcrumbs=array(
			 'Best Practice'=>array('bestpractice/index'),
			 'New Best Practice',
			 );
?>

<?php 
 //Print errors if there is one
print (isset($error)) ? '<div class="alert alert-error">' . $error . '</div>' : ""; 
?>

<h1>Suggest a Best Practice</h1>

<p>
Best Practices are techniques and methods that produce better results than the naive approach, that programmers should try and follow.
</p>

<p>
Are there any best practices that you have followed that have worked wonders for you? Suggest it below!
</p>

  <br/>

  <form id="frmSuggestBestPractice" action="<?php echo Yii::app()->createUrl('bestpractice/create')?>" method="post">

  <label for="frmSuggestBestPractice-field-name">Best Practice Name</label>
  <input type="text" placeholder="Name" id="frmSuggestBestPractice-field-name" name="BestPractice[name]" />
  <br/>

  <label for="frmSuggestBestPractice-field-description">Best Practice Description</label>
  <textarea rows="10" cols="50" placeholder="Description" id="frmSuggestBestPractice-field-description" name="BestPractice[description]"></textarea>
  <br/>

  <label for="frmSuggestBestPractice-field-snippet">Code Snippet</label>
  <textarea rows="10" cols="50" placeholder="Snippet" id="frmSuggestBestPractice-field-snippet" name="BestPractice[snippet]"></textarea>
  <br/>

  <label for="frmSuggestBestPractice-field-ref_url">Best Practice Reference URL</label>
  <input type="text" placeholder="Reference URL" id="frmSuggestBestPractice-field-ref_url" name="BestPractice[ref_url]" />
  <br/>

  <label for="frmSuggestBestPractice-field-context">Related Context</label>
  <select id="frmSuggestBestPractice-field-context" name="context">
    <?php 
       $context_list = Context::model()->findAll();
    foreach ($context_list as $context_obj){    
      print "<option" . (($context_obj->name === $context) ? " selected=\"selected\"" : "") . ">";
      print $context_obj->name . "</option>\n"; 
      }   
    ?>
  </select>
  <br/>

  <br/>
  <button type="submit" class="btn btn-large btn-success" id="btnSubmitBestPractice">Submit Best Practice</button>
  </form>

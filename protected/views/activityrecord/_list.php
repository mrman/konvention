<?php
   /*ActivityRecord widget
   *Possibly expecting:
   *@var $model The model the ActivityRecord object is related to
   */

   //Load the recent activities (most recent first)
$activity_list = ActivityRecord::model()->findAll(array('limit'=>5, 'order'=>'timeof DESC'));
$disable_load = (count($activity_list) < 5) ? 'disabled="disabled"' : '';

?>

<table class="table" id="recent_activity">
  <thead>
    <tr>
      <td><b class="large">Recent Activity</b></td>
    </tr>
  </thead>
  <tbody>
    <?php foreach($activity_list as $activity): ?>
    <tr>
      <td> <?php 
     $username_safe = htmlspecialchars($activity->username);
     $desc_safe = htmlspecialchars($activity->desc);
     $url_safe = htmlspecialchars($activity->url);
     $subject_safe = htmlspecialchars($activity->subject);

     print "<a href=\"" . Yii::app()->createUrl("user/view/$username_safe") . "\">$username_safe</a> ";
     print "$desc_safe <a href=\"$url_safe\">$subject_safe</a>"; 
     print " - <time class=\"timeago\" id=\"activity_timeof\" datetime=\"" . $activity->timeof . "\"></time>";
	?></td>
    </tr>
    <? endforeach ?>    
  </tbody>
</table>
    <a href="<?php echo Yii::app()->createUrl('site/getrecentactivity'); ?>" data-list="recent_activity" data-offset="5" data-limit="5" class="btn btn-info ajaxListExtender" <?php echo $disable_load; ?>>Load more...</a>
     <button class="btn btn-inverse btnHideTableBody" data-target="recent_activity">Hide All</button>
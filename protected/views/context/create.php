<?php
/*
 * @var $recaptcha_html String HTML for recaptcha plugin
 */
   
   $this->breadcrumbs=array(
			    'Context'=>array('context/index'),
			    'Suggest a Context',
			    );
?>
<h1>Suggest a Context</h1>

<p>
  Contexts are the areas of expertise or practice that have best practices and conventions. Best practices and conventions cannot exist without a context, so if you have one to add, suggest it below! 
</p>

<p>
  Contexts that have gained a significant amount of suggestions will be automatically added.
</p>
<br/>
<form id="frmSuggestContext">
  <label for="frmSuggestContext-field-name">Context Name<sup class="required-field">*</sup></label>
  <input type="text" placeholder="Context Name" name="ContextSuggestion[name]" />
  <br/>

  <label for="frmSuggestContext-field-description">Context Description<sup class="required-field">*</sup></label>
  <textarea placeholder="Context Description" name="ContextSuggestion[description]"></textarea>
  <br/>

  <label for="frmSuggestContext-field-ref_url">Reference URL<sup class="required-field">*</sup></label>
  <input type="text" placeholder="Reference URL" name="ContextSuggestion[ref_url]" />
  <br/>

  <label for="frmSuggestContext-field-comment">Comments</label>
  <textarea placeholder="Why is this context important?" name="ContextSuggestion[comment]" ></textarea>
  <br/>

  <?php echo $recaptcha_html ?>

	<br/>
  <a href="<?php echo Yii::app()->createUrl('context/suggestnew'); ?>" class="btn btn-large btn-success ajaxSubmitButton" id="btnSubmitContextSuggestion">Submit Context Suggestion</a>
</form>

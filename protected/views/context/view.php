<?php
   /* @var $this ContextController */
   /* @var $model Context */
   /* @var $best_practice_list List of Best Practices related to this Context */

   $this->breadcrumbs=array(
			    'Context'=>array('context/index'),
			    $model->name,);

?>

<h1><?php echo $model->name ?><a href="<?php echo Yii::app()->createUrl('bestpractice/create', array('context'=>$model->name))?>" class="btn btn-info btn-large pull-right" id="btnSuggestBestPractice"><i class="icon-leaf icon-white"></i> Suggest a new Best Practice</a></h1>

<br/>
<br/>
<dl class="dl-horizontal">

  <dt>Description</dt>
  <dd><?php echo $model->description ?></dd>
  <br/>

  <dt>Reference URL</dt>
  <dd><a href="<?php echo $model->ref_url ?>"><?php echo $model->ref_url ?></a></dd>  
  <br/>
</dl>


<h2 class="page-section-header">Best Practices</h2>
<table data-src="<?php echo Yii::app()->createUrl('bestpractice/list',array('context'=>$model->name)); ?>" class="table table-condensed table-bordered" id="bp_list">
  <thead>
    <tr>
      <th>Name</th>
      <th>Context</th>
      <th>Description</th>
      <th>Reference URL</th>
      <th>Likes</th>  
    </tr>
  </thead>
  <tbody> 
  </tbody>
</table>

<br />
<br />


<?php
   /* @var $this ContextController */
   /* @var $context_list List of Contexts */

   $this->breadcrumbs=array(
			    'Context',
			    );
?>
<h1 class="page-section-header">Available Contexts<a href="<?php echo Yii::app()->createUrl('context/create')?>" class="btn btn-info btn-large pull-right" id="btnSuggestContext"><i class="icon-leaf icon-white"></i> Suggest a new Context</a></h1>

<br />
<br />

<table class="table table-condensed table-bordered" id="context_list">
  <thead>
    <tr>
      <th>Name</th>
      <th>Description</th>
      <th>Reference URL</th>
    </tr>
  </thead>
  <tbody> 
  </tbody>
</table>

<br />
<br />

<h1 class="page-section-header">Suggested Contexts</h1>
<br />
<br />
<table class="table table-condensed table-bordered" id="suggested_context_list">
  <thead>
    <tr>
      <th>Name</th>
      <th>Description</th>
      <th>Reference URL</th>
    </tr>
  </thead>
  <tbody>
    
  </tbody>
</table>


<br />
<br />
<br />
<br />

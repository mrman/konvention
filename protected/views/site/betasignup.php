<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $status String String that tells us whether feedback was submitted, might NOT be present */

//For authenticated users, the email field will be their email on file, and will be disabled
$email_attribs = "";
if (!Yii::app()->user->isGuest){
  $email_attribs = 'value="' . Yii::app()->user->email . '"';
}

//Breadcrumbs
$this->breadcrumbs=array( 'Register for Beta',);

?>

<h2>New User registration is currently down</h2>

<p> 
  As Konvention is currently undergoing closed alpha testing, User registration is not allowed at this time.
</p>
<p>
  If you're interested in being on of the users in the Beta test, please enter your email below:
</p>

<br/>
<div class="input-append">
  <form id="frmBetaSignup" action="<?php echo Yii::app()->createUrl('site/betasignup'); ?>" method="POST">
    <input type="text" placeholder="Your email address" name="email"/>
    <a href="<?php echo Yii::app()->createUrl('site/betasignup') ?>" class="btn btn-success ajaxSubmitButton">Sign Up</a>
  </form>
</div>






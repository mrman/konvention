<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
$this->breadcrumbs=array(); //Only Home in the breadcrumbs

//Create random placeholder
$search_placeholder_arr = array("RESTful API Design", 
				"JSON", 
				"Data interchange", 
				"Web actors", 
				"Parallel execution", 
				"Database Transactions");
$search_placeholder = $search_placeholder_arr[rand(0,count($search_placeholder_arr) - 1)];			
?>

<br/>
<h1 class="pageTitle">Find development-proven conventions and best practices: </h1>
<div id="searchWell">
  <h1>Search</h1>
  <form id="frmKonventionSearch" method="GET" action="<?php echo Yii::app()->createUrl('search'); ?>">
    <input type="text" id="frm-input-query" name="query" placeholder="Ex. <?php print $search_placeholder; ?>" />
    <select id="frm-select-context" name="context">  
      <option value="all">&lt;All Contexts&gt;</option>
      <?php foreach ($context_list as $context) {print "<option value=\"$context->name\">$context->name</option>\n"; } ?>
    </select>
    <button type="submit" class="btn btn-primary btn-large" id="btnSearch">Search</button>
  </form>

  <a href="<?php echo Yii::app()->createUrl('context/index'); ?>" class="large">Contexts</a>
</div>

<br/>
<div class="well well-small">
  <?php $this->renderPartial("/activityrecord/_list"); ?>
</div>  

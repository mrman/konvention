<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $recaptcha_html String HTML for recaptcha plugin */
/* @var $status String String that tells us whether feedback was submitted, might NOT be present */


//For authenticated users, the email field will be their email on file, and will be disabled
$email_attribs = "";
if (!Yii::app()->user->isGuest){
  $email_attribs = 'value="' . Yii::app()->user->email . '"';
}

//Breadcrumbs
$this->pageTitle=Yii::app()->name . ' - Contact Us';
$this->breadcrumbs=array( 'Contact Us',);

?>

<h1>Contact Us</h1>

<p class="large"> 
Do you have any comments or questions about the app? See something unforgivably incorrect? Have some features you want to see implemented?
<br/>
Pour your heart into this form and send it on it's merry way!
</p>

<br/>
<form id="frmContact" action="<?php echo Yii::app()->createUrl('site/contact'); ?>" method="POST">

  <label for="frmContact-field-email">Email<sup class="required-field">*</sup></label>
  <input type="text" placeholder="Your email address" name="Feedback[user_email]" <?php echo $email_attribs ?>/>
  <br/>

  <label for="frmContact-field-comment">Comments<sup class="required-field">*</sup></label>
  <textarea placeholder="Your comments, suggestions, and criticism goes here" id="frmContact-field-comment" name="Feedback[comments]"></textarea>
  <br/>

  <?php echo $recaptcha_html ?>
  
  <br/>
  <a href="<?php echo Yii::app()->createUrl('site/submitfeedback') ?>" class="btn btn-success btn-large ajaxSubmitButton">Submit Feedback</a>
</form>






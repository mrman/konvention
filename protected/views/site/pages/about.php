<?php
   /* @var $this SiteController */

   $this->pageTitle=Yii::app()->name . ' - About';
$this->breadcrumbs=array(
'About',
);
?>
<h1>About Konvention</h1>

<p class="large">So you've just read a guide/blog/website/book on your favorite language, and you're ready to start writing applications and proving your worth. Where do you find the <b>best practices</b>, <b>conventions</b> preferred in your community, <b>common pitfalls</b>, <b>problems</b>, <b>limitations</b>? Sure documentation is always a good start, but what if your situation's not in there? <a href="http://lmgtfy.com/?q=scraping+blogs+to+find+scraps+of+convention">Google</a>?</p>

<p class="large">Introducing Konvention, a web app made to achieve a simple goal. Centralize Best Practices and Conventions for programmers (web and other wise). As we move on and grow, other features may change, but this is our purpose.</p>

<h3>Questions? Comments? <br/> <a href="<?php echo $this->createUrl("site/contact") ?>" class="btn btn-info"> Contact Us </a> </h3>


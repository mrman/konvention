<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $newAcct Boolean This tells us if an account has just been created */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array('Login');

	/*Form hint markup
	  <p class="hint">
	  Hint: You may login with <kbd>demo</kbd>/<kbd>demo</kbd> or <kbd>admin</kbd>/<kbd>admin</kbd>.
	  </p>*/

?>

<?php if (isset($_GET['new'])): ?>
<div class="alert alert-success">Your account has been created!
<br/> 
   Login below:</div>
  <?php endif; ?>

<h1>Login</h1>

<p>Please enter your username and password below:</p>


<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'frmLoginUser',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username'); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="row rememberMe">
		<?php echo $form->checkBox($model,'rememberMe'); ?>
		<?php echo $form->label($model,'rememberMe'); ?>
		<?php echo $form->error($model,'rememberMe'); ?>
	</div>

	<div class="row buttons">
  <?php echo CHtml::submitButton('Log In', array('class'=>'btn btn-large btn-success')); ?>
	</div>

<?php $this->endWidget(); ?>
</div>
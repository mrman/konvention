<?php
   /* @var $this ContextsuggestionController */
   /* @var $model ContextSuggestion */

   $this->breadcrumbs=array(
			    'Suggested Context'=>array('context/index'),
			    $model->name,
			    );
?>


<h1><span class="indicator-urgent">Suggested Context:</span> <?php echo $model->name ?></h1>
     <p class="indicator-urgent">This Context has been suggested, but is not officially supported by Konvention, Vote for it if you think it ought to be included!</p>
   <?php $this->renderPartial("/karma/_widget", array('model'=>$model,'karma'=>$model->getKarma())); ?>

<br/>
<br/>
<?php if (Yii::app()->user->name == 'vados'): ?>
<a href="<?php echo Yii::app()->createUrl('contextsuggestion/promote',array('contextsuggestion_id'=>$model->getPrimaryKey())) ?>" data-doonce="true" class="btn btn-large btn-success ajaxSimpleButton"><i class="icon-star icon-white"></i> Promote Suggestion</a>
<br/>
<br/>
   <?php endif;?>

<dl class="dl-horizontal">

  <dt>Description</dt>
  <dd><?php echo $model->description ?></dd>
  <br/>

  <dt>Reference URL</dt>
  <dd><a href="<?php echo $model->ref_url ?>"><?php echo $model->ref_url ?></a></dd>  
  <br/>

  <dt>Creator's comments</dt>
  <dd><?php echo $model->comment ?></dd>
  <br/>

</dl>

<?php $this->renderPartial("/comment/_list", array('model'=>$model)); ?>

<div class="comments-section">
  <h3>Comments</h3>
  <div id="comment-list">
    <?php
       //Load comments
       $comment_list = Comment::model()->findAllByAttributes(array('type'=>$model->tableName(),'fk_type_id'=>$model->getPrimaryKey()));
    if (count($comment_list) == 0) {
      print '<div id="no-comments">It seems that there are no comments on this ' . strtolower($model->semanticName()) . ' yet. ';
      print "" . ((Yii::app()->user->isGuest) ? "<a href=\"" . Yii::app()->createUrl('site/login') . "\">Login</a> and s" : "S") . 'tart the conversation! </div>';
    } else {
      $comment_id = 0;
      foreach ($comment_list as $comment){
	$username_safe = htmlspecialchars($comment->username);
	$comment_safe = htmlspecialchars($comment->comment);
	print "<div class=\"comment\" id=\"comment-" . $comment_id . "\">\n";
	print "<div class=\"comment-header\">\n";
	print "<span id=\"username\"><a href=\"" . (($username_safe === "Guest") ? "#" : Yii::app()->createUrl("user/view/$username_safe")) . "\">$username_safe</a></span>\n";
	print " - Posted <time class=\"timeago\" id=\"submitted_at\" datetime=\"" . $comment->submitted_at . "\"></time>";
	print "</div>"; //End of header
	print "<div class=\"comment-body\">\n";
	print "<p id=\"comment-text\">$comment_safe</p>\n";
	print "</div>\n";//End of comment body
	print "</div>\n";
	$comment_id++;
      }

    }
    ?>
  </div>
  <div id="comment-submit">
    <div id="commentSubmissionResult"></div>
      <?php if (!Yii::app()->user->isGuest): ?>
      <div id="commentSubmissionResult"></div>
      <textarea placeholder="Enter your comment here" id="comment-textarea"></textarea>
      <a href="<?php echo Yii::app()->createUrl(strtolower($model->tableName()) . '/submitcomment', array("id"=>$model->getPrimaryKey())); ?>"  id="btnSubmitComment" class="btn btn-success btn-large">Submit Comment</a>
      <?php endif; ?>
  </div>
</div>

<br/>
<br/>

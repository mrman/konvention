<?php
/*Karma widget
 *Expecting:
 *@var $model The model the Karma object belongs to
 *@var $karma Karma object to use to calculate
 */
$likes = $karma->likes;
$dislikes = $karma->dislikes;
$no_input = $likes == 0 && $dislikes == 0;

$total = $likes + $dislikes;
$percent_likes = ($no_input) ? 50 : (($likes/$total) * 100);
$percent_dislikes = ($no_input) ? 50 : (($dislikes/$total) * 100);

//Attempt to retrieve the karma object and disable/enable the buttons
$karmarecord = KarmaRecord::model()->findByAttributes(array(
							    'user_id'=>Yii::app()->user->id,
							    'type'=>$model->tableName(),
							    'fk_type_id'=>$model->getPrimaryKey()));

//Disable like/dislike buttons based on karma records for the given user
$no_like = '';
$no_dislike ='';
if (!($karmarecord === NULL)){
  $no_like = ($karmarecord->opinion === 'LIKE') ? 'disabled="disabled"': '';
  $no_dislike = ($karmarecord->opinion === 'DISLIKE') ? 'disabled="disabled"' : '';
} else if (Yii::app()->user->isGuest){ // Disable everything if user is a guest
  $no_like = 'disabled="disabled"';
  $no_dislike = 'disabled="disabled"';
  }
							    

$difference = $likes - $dislikes;
$popover_content = "Likes: $karma->likes | Dislikes: $karma->dislikes";
?>

<div class="btn-group" id="widget-karma">
  <a href="<?php echo Yii::app()->createUrl($model->tableName() . "/addkarma/" . $model->getPrimaryKey()); ?>?opinion=LIKE" class="btn btn-success ajaxSubmitButton" id="btnLike" <?php echo $no_like; ?>><i class="icon-thumbs-up icon-white"></i> Yup</a>
  <button class="btn" id="differential" rel="popover"><span class="ajaxLiveField" data-field="karma" data-src="<?php echo Yii::app()->createUrl($model->tableName() . "/getkarma/" . $model->getPrimaryKey()); ?>"><?php echo $difference; ?></span></button>
  <a href="<?php echo Yii::app()->createUrl($model->tableName() . "/addkarma/" . $model->getPrimaryKey()); ?>?opinion=DISLIKE" class="btn btn-danger ajaxSubmitButton" id="btnDislike" <?php echo $no_dislike; ?>><i class="icon-thumbs-down icon-white"></i> Nope</a>
</div>

<div id="differential-distribution" hidden="hidden">
  <div class="progress">
  <div class="bar bar-success" style="width: <?php echo $percent_likes; ?>%;"><?php echo $likes . (($likes == 1) ? " like" : " likes");?> </div>
    <div class="bar bar-danger" style="width: <?php echo $percent_dislikes; ?>%;"><?php echo $dislikes . (($dislikes == 1) ? " dislike" : " dislikes");?> </div>
  </div>
</div>




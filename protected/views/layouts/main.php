<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" />
    <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/konventionLogo.ico" />

    <!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

    <!--[if gte IE 9]>
	<style type="text/css">
	  .gradient {
	  filter: none;
	  }
	</style>
	<![endif]-->

<?php 
	  /*    <!-- CSS Includes -->
    <!-- Twitter Bootstrap -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/plugins/bootstrap/css/bootstrap.min.css" />
    <!-- Datatables -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/plugins/datatables/media/css/demo_table.css" />
    <!-- Google Prettify -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/plugins/google-code-prettify/prettify.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/plugins/google-code-prettify/sons-of-obsidian.css" />
    <!-- Icons, via Adam Fairhead @ http://fairheadcreative.com -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/plugins/webicons/fc-webicons.css" />
    <!-- Custom -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
	  */ ?>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/cssbundle-dev.css"/>

    <title>Konvention</title>
  </head>

  <body>

    <div id="container">

      <!-- Page Header -->
      <div id="header">
	<div id="logo">	    
	  <div id="title"><a href="<?php echo $this->createUrl('site/index') ?>"><?php echo CHtml::encode(Yii::app()->name); ?><sup>&alpha;</sup></a></div>
	  <span id="subtitle"><?php $tagline_arr = array("Consistency is our major export.", 
						  "Helping you write less terrible code.", 
						  "Hoping our consistency will please the robot overlords.", 
						  "Helping you stop reinventing the wheel.", 
						  "How to do it right.", 
						  "So others can read your code.");print $tagline_arr[rand(0,count($tagline_arr) - 1)];?> 
	  </span>
	</div>

	<div id="topnav">
	  <ul>
	    <li><a href="<?php echo $this->createUrl("site/index") ?>">Search</a></li>
	    <li><a href="<?php echo $this->createUrl("site/page", array('view'=>'about')) ?>">About</a></li>

	    <?php if (Yii::app()->user->isGuest): ?>
	    <li><a href="<?php echo $this->createUrl("site/login") ?>">Login</a></li>
	    <li><a href="<?php echo $this->createUrl("user/create") ?>">Register</a></li>
	    <?php else: ?>
	    <li><a href="<?php echo $this->createUrl("user/view/" . Yii::app()->user->name) ?>">Profile</a></li>
	    <li><a href="<?php echo $this->createUrl("site/logout") ?>">Logout</a> [<?php echo Yii::app()->user->name ?>]</li>
	    <?php endif; ?>

	  </ul>
	</div>
      </div>								  
      <!-- End of Page Header -->

      <!-- Page content -->
      <div id="page">
	<div id="alert-page" hidden="hidden">
	  <button type="button" class="close" data-dismiss="alert">×</button>
	  <p id="message"></p>
	</div>
	<!-- Breadcrumbs -->
     <?php if(isset($this->breadcrumbs)){$this->widget('zii.widgets.CBreadcrumbs', array('separator'=>'<i class="icon-chevron-right"></i> ','links'=>$this->breadcrumbs,));}?>

	<?php echo $content; ?>
      </div><!-- End of page content -->

      <!-- Footer -->
      <div id="footer">
	Copyright &copy; <?php echo date('Y'); ?> by <a href="http://www.vadosware.com">Vadosware</a>.
	<br/>
	All Rights Reserved.
	<br/>
	Powered by <a href="http://www.yiiframework.com/">Yii</a> and <a href="http://twitter.github.com/bootstrap">Twitter Bootstrap</a><br/>
      </div>
      <!-- End of footer -->


    </div><!-- End of pageWrapper -->

    <!-- JS Includes -->
<?php
										     /*    <!-- jQuery -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery/jquery-1.8.2.min.js" type="text/javascript"></script>
    <!-- Twitter Bootstrap -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- Datatables -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/plugins/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <!-- Google Prettify -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/plugins/google-code-prettify/prettify.js" type="text/javascript"></script>
    <!-- jTruncate -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/plugins/jTruncate/jquery.jtruncate.min.js" type="text/javascript"></script>
    <!-- timeago -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/timeago/jquery.timeago.js" type="text/javascript"></script>
   <!-- Modernizr (required for icons) -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/modernizr.js" type="text/javascript"></script>
    <!-- Custom -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/main.js" type="text/javascript"></script>
										     */ 
										     ?>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jsbundle-dev.js" type="text/javascript"></script>
										     
  </body>
</html>

<?php

class SiteController extends Controller
{
  /**
   * Declares class-based actions.
   */
  public function actions()
  {
    return array(
		 // captcha action renders the CAPTCHA image displayed on the contact page
		 'captcha'=>array(
				  'class'=>'CCaptchaAction',
				  'backColor'=>0xFFFFFF,
				  ),
		 // page action renders "static" pages stored under 'protected/views/site/pages'
		 // They can be accessed via: index.php?r=site/page&view=FileName
		 'page'=>array(
			       'class'=>'CViewAction',
			       ),
		 );
  }

  /**
   * This is the default 'index' action that is invoked
   * when an action is not explicitly requested by users.
   */
  public function actionIndex()
  {
    // renders the view file 'protected/views/site/index.php'
    // using the default layout 'protected/views/layouts/main.php'
    $context_list = Context::model()->findAll(array('order'=>'name')); //Get all the contexts (for the list)
    $this->render('index', array('context_list'=>$context_list));	  
  }

  /**
   * This is the action to handle external exceptions.
   */
  public function actionError()
  {
    if($error=Yii::app()->errorHandler->error)
      {
	if(Yii::app()->request->isAjaxRequest)
	  echo $error['message'];
	else
	  $this->render('error', $error);
      }
  }

  /**
   * Displays the contact page
   */
  public function actionContact()
  {
    //Import the recaptcha lib
    Yii::import('ext.recaptcha.recaptchalib', true);

    //Public key from recaptcha
    $publickey = "6LcoRdkSAAAAAAxbwbugtOI1mtXBtdBnaRqI4OOJ"; // you got this from the signup page    

    $this->render('contact',array('recaptcha_html'=>recaptcha_get_html($publickey)));
  }

  /**
   * Displays the Beta signup page
   */
  public function actionBetaSignup()
  {
    if (Yii::app()->request->isAjaxRequest){
      //JSON Response Headers
      header('Content-Type: application/json; charset="UTF-8"');    
      $form_data = array();
      parse_str($_REQUEST['form_data'],$form_data);

      if (isset($form_data['email'])){
	// Clean input
	$email = escapeshellarg($form_data['email']);
	//Add to the beta list      
	`echo $email >> beta-list.txt`;
	echo json_encode(array('status'=>'success',
			       'message'=>'Thanks for applying to be in the alpha test! When testing begins, you will be contacted.'));
      } else {	
	echo json_encode(array('status'=>'error',
			       'message'=>'Sorry, your email could not be added to the mailing list... please wait a while and try again, or contact us directly.'));
      }

    } else {      
      $this->render('betasignup');
    }
  }



  /**
   * AJAX Submission of feedback from contact page
   */
  public function actionSubmitFeedback(){
    //Import the recaptcha lib
    Yii::import('ext.recaptcha.recaptchalib', true);

    //Ensure the requext is ajax 
    if (!YII_DEBUG && !Yii::app()->request->isAjaxRequest) {
      throw new CHttpException('403', 'Forbidden access.');
    }
	  	  
    //JSON Headers
    header('Content-Type: application/json; charset="UTF-8"');    

    if (isset($_POST['form_data'])){
      //Get form data into an array
      $form_data = array();
      parse_str($_POST['form_data'], $form_data); //extract from data into php array
      $feedback = $form_data['Feedback'];
      
      //Validate the feedback (ensure what is needed is there) then
      //Do some stuff to send out feedback/validate emails, etc here


      //Test Recaptcha
      $privatekey = "6LcoRdkSAAAAAJFfPYBtDdo_x0kaqXy_Ei0ymjRC";
      $resp = recaptcha_check_answer ($privatekey,
				      $_SERVER["REMOTE_ADDR"],
				      $_POST["recaptcha_challenge_field"],
				      $_POST["recaptcha_response_field"]);
       
      if (!$resp->is_valid){
    	//reCaptcha failed -- exit early
    	echo json_encode(array('status'=>'error',
			       'message'=>'Invalid reCaptcha entry -- Please re-enter the Captcha and try again'));
    	return;
      }

      //Send feedback email
      try {
	Yii::app()->mailer->Host = 'mail.vadosware.com';
	Yii::app()->mailer->IsSMTP();
	Yii::app()->mailer->From = $feedback['user_email'];
	if (Yii::app()->user->isGuest)
	  Yii::app()->mailer->FromName = 'Anonymous User';
	else
	  Yii::app()->mailer->FromName = 'User: ' . Yii::app()->user->name;
	Yii::app()->mailer->AddReplyTo($feedback['user_email']);
	Yii::app()->mailer->AddAddress('contact@vadosware.com');
	Yii::app()->mailer->Subject = '[Konvention] App Feedback';
	Yii::app()->mailer->Body = nl2br($feedback['comments']);
	Yii::app()->mailer->IsHtml(true);
	Yii::app()->mailer->Send();
      
	//Render with appropriate status
    	echo json_encode(array('status'=>'success',
			       'message'=>'Your feedback was successfully submitted! Thanks for contributing to Konvention.'));
      } catch(phpmailerException $e){
	echo json_encode(array('status'=>'error',
			       'message'=>'An error occurred while submitting your feedback... please try again later. Or send an email to contact@vadosware.com directly'));
      } catch (Exception $e) {
	echo json_encode(array('status'=>'error',
			       'message'=>'An error occurred while submitting your feedback... please try again later. Or send an email to contact@vadosware.com directly'));

      }
    } else {
      //No form data submitted
      echo json_encode(array('status'=>'error',
			     'message'=>'Your feedback could not be submitted... Please ensure that all fields are correctly filled'));      
    }
  }


  /**
   * AJAX retrieval of recent activity
   */
  public function actionGetRecentActivity(){
    //Ensure the requext is ajax 
    if (!YII_DEBUG && !Yii::app()->request->isAjaxRequest) {
      throw new CHttpException('403', 'Forbidden access.');
    }

    //Quit if offset and limit are not set
    if (!(isset($_POST['offset']) && isset($_POST['limit']))){
      throw new CHttpException('400', 'Invalid request.');      
    }

    //Gather criteria (offset and limit)
    $criteria = array('order'=>'timeof DESC','offset'=>intval($_POST['offset']), 'limit'=>intval($_POST['limit']));

    //Get activities with given offset and limit
    $activity_list = ActivityRecord::model()->findAll($criteria);
    $result = array('status'=>'success','list'=>array());

    //Indicate whether all records have been pulled
    if (count($activity_list) < $criteria['limit'])
      $result['finished'] = 1;

    foreach ($activity_list as $activity){
      $username_safe = htmlspecialchars($activity->username);
      $desc_safe = htmlspecialchars($activity->desc);
      $url_safe = htmlspecialchars($activity->url);
      $subject_safe = htmlspecialchars($activity->subject);
      
      $activity_str = "<a href=\"" . Yii::app()->createUrl("user/view/$username_safe") . "\">$username_safe</a> $desc_safe <a href=\"$url_safe\">$subject_safe</a>";
      $activity_str = $activity_str . " - <time class=\"timeago\" id=\"activity_timeof\" datetime=\"" . $activity->timeof . "\"></time>"; 
      array_push($result['list'],$activity_str);      
    }

    //JSON Headers
    header('Content-Type: application/json; charset="UTF-8"');    
    echo json_encode($result);

  }




  /**
   * Displays the login page
   */
  public function actionLogin()
  {
    $model=new LoginForm;

    // if it is ajax validation request
    if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
      {
	echo CActiveForm::validate($model);
	Yii::app()->end();
      }

    // collect user input data
    if(isset($_POST['LoginForm']))
      {
	$model->attributes=$_POST['LoginForm'];
	// validate user input and redirect to the previous page if valid
	if($model->validate() && $model->login())
	  $this->redirect(Yii::app()->user->returnUrl);
      }
    // display the login form
    $this->render('login',array('model'=>$model));
  }

  /**
   * Logs out the current user and redirect to homepage.
   */
  public function actionLogout()
  {
    Yii::app()->user->logout();
    $this->redirect(Yii::app()->homeUrl);
  }
}
<?php

class CommentController extends Controller
{
	public function filters(){
		return array('accessControl');
	}
	
	  public function accessRules()
  {
    return array(
		 array('allow', //Allow only authenticated users to create and comment 
		       'actions'=>array('addcommentajax'),
		       'users'=>array('@'),
		       ),
		 array('allow', // allow admin user do anything they feel like.
		       'users'=>array('vados'),
		       ),
		 array('deny',  // deny all users
		       'users'=>array('*'),
		       ),
		 );
  }

	public function actionAddCommentAjax()
	{
	  //Create a new comment and populate it with http headers
	  $new_comment = new Comment;
	  $new_comment->attributes = $_POST['Comment'];

	  //Attempt to save the new comment
	  if ($new_comment->save()){
	    $this->renderPartial('_commentSubmissionResult', array('status'=>'success',
								   'alert_class'=>'alert alert-success',
								   'message'=> 'Comment Saved'), false, true);
	  } else {
	    $this->renderPartial('_commentSubmissionResult', array('status'=>'error',
								   'message'=> 'Uh-oh, it looks like your comment could not be saved... Please wait a while and try again'), false, true);
	  }
	}

/*	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
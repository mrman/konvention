<?php

class SearchController extends Controller
{
	public function actionIndex()	{
	  //Ensure both query and context are provided, else redirect
	  if (isset($_REQUEST['query']) && 
	      isset($_REQUEST['context'])){

	    //Do a simple search for the given query term under the given context (search() returns  data providers)	   
	    //$dp_contexts = Context::model()->search($_REQUEST['query'],$_REQUEST['context']);
	    
	    //Do a simple search for the given query term under the given context
	    $results = array();
	    $results['bestpractices'] = BestPractice::model()->search($_REQUEST['query'],$_REQUEST['context'])->getData();
	    
	    $this->render('index', array('results' => $results));
	  } else {
	    $this->redirect('site');
	  }
	}

	// -----------------------------------------------------------
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
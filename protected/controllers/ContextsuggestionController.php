<?php

class ContextsuggestionController extends Controller
{
  public function filters()
  {
    // return the filter configuration for this controller, e.g.:
    return array('accessControl');
  }

  /**
   * Specifies the access control rules.
   * This method is used by the 'accessControl' filter.
   * @return array access control rules
   */
  public function accessRules()
  {
    return array(
		 array('allow',  // allow all users to perform 'index' and 'view' actions
		       'actions'=>array('view','viewbyname','list'),
		       'users'=>array('*'),
		       ),
		 array('allow', //Allow only authenticated users to update 
		       'actions'=>array('view','viewbyname','submitcomment','addkarma','list'),
		       'users'=>array('@'),
		       ),
		 array('allow', // allow admin user do anything they feel like.		       
		       'users'=>array('vados'),
		       ),
		 array('deny',  // deny all users
		       'users'=>array('*'),
		       ),
		 );
  }

  /*
   * Produce list of context suggestions on ajax request (expecting insertion into datatables data)
   */
  public function actionPromote($contextsuggestion_id){
    //If JSON request, produce listing, else produce html
    if (!YII_DEBUG && !Yii::app()->request->isAjaxRequest)
      throw new CHttpException('403','Forbidden access.');
    
    //Set JSON header
    header('Content-Type: application/json; charset="UTF-8"');        

    //Promote the given context suggestion to a real context
    $context_suggestion = $this->loadModel($contextsuggestion_id);

    //Quit if a context with the same name exists
    if (Context::model()->exists('name = :name',array('name'=>$context_suggestion->name))){
      $success = $context_suggestion->delete(); //Ensure that the context suggestion is deleted
      echo json_encode(array('status'=>'error',
			     'message'=> 'It looks like there is already a Context with this name, No duplicate Contexts are supported...'));
      return;
    }
    
    //Create the new context, replicate context suggestion info, and delete the old suggestion
    $context = new Context;
    if ($context->slurpSuggestion($context_suggestion) && $context->save()){
      //Record an activity for the new comment
      $activityrecord = new ActivityRecord;
      $new_url = Yii::app()->createUrl(strtolower($context->tableName()) . '/view/' . $context->name);
      $activityrecord->attributes = array('username'=>Yii::app()->user->name,
    					  'desc'=>'promoted a suggested context',
    					  'fk_type'=>$context->tableName(),
    					  'fk_type_id'=>$context->context_id,
    					  'url'=> $new_url,
    					  'subject'=>$context->name);
      $activityrecord->save();

      //Delete the old activity record relating to the creation of the Context Suggestion
      ActivityRecord::model()->deleteAllByAttributes(array('fk_type_id'=>$context_suggestion->contextsuggestion_id,
									       'desc'=>'suggested a new context'));

      //Delete the context suggestion
      $context_suggestion->delete();

      echo json_encode(array('status'=>'success',
    			     'message'=> 'Context suggestion has been successfully promoted!'));
    } else {
      echo json_encode(array('status'=>'error',
    			     'message'=> 'Context suggestion could not be promoted...'));
    }
  }

  /*
   * Produce list of context suggestions on ajax request (expecting insertion into datatables data)
   */
  public function actionList(){
    //If JSON request, produce listing, else produce html
    if (!YII_DEBUG && !Yii::app()->request->isAjaxRequest) {
      throw new CHttpException('403', 'Forbidden access.');	  
    } else {
      //JSON Headers
      header('Content-Type: application/json; charset="UTF-8"');    
	  
      $contextsuggestion_list = ContextSuggestion::model()->findAll(array('order'=>'name'));
      $result = array('aaData'=>array(), 'aaColumns'=>array(),'aaSorting'=>array());
	  
      //Add data
      foreach ($contextsuggestion_list as $contextsuggestion){
	array_push($result['aaData'], array($contextsuggestion->name,
					    $contextsuggestion->description,
					    $contextsuggestion->ref_url));
      }
	  
      echo json_encode($result);
    }
  }

  public function actionView($id)
  {
    $this->render('view', array('model'=>$this->loadModel($id)));
  }

  /**
   * Look up and display a context's data by context name
   * @param string $contextsuggestionname The contextname of the Context to be retrieved
   */
  public function actionViewByName($contextsuggestionname)
  {
    $model = $this->loadByName($contextsuggestionname);
    $this->render('view',array('model'=>$model));
  }

  /**
   * Returns the Context Suggestion based on the primary key given in the GET variable.
   * If the data model is not found, an HTTP exception will be raised.
   * @param integer context suggestionname The context suggestionname of the model to be loaded
   */
  public function loadByName($contextsuggestionname)
  {
    $model=ContextSuggestion::model()->findByAttributes(array('name'=>$contextsuggestionname));
    if($model===null)
      throw new CHttpException('404','The requested page does not exist.');
    return $model;
  }


  /**
   * AJAX action that attempts to add a comment to a given Best Practice Model (identified by id)
   * @param integer $id the ID of the model to which the comment belongs
   */
  public function actionSubmitComment($id) {
    
    if (!YII_DEBUG && !Yii::app()->request->isAjaxRequest) {
      throw new CHttpException('403', 'Forbidden access.');
    }
	  	  
    //JSON Headers
    header('Content-Type: application/json; charset="UTF-8"');

    //Create a new comment and populate it with http headers
    $new_comment = new Comment;
    if (isset($_POST['comment'])) {
      $new_comment->username = Yii::app()->user->name;
      $new_comment->type = "ContextSuggestion";
      $new_comment->fk_type_id = $id;
      $new_comment->comment = $_POST['comment']; //This needs to be escaped
    }
      
    //Attempt to save the new comment
    if ($new_comment->save()){     
      echo json_encode(array('status'=>'success',
			     'message'=> 'Comment Saved'));
    } else {
      echo json_encode(array('status'=>'error',
			     'message'=> 'Uh-oh, it looks like your comment could not be saved... Please wait a while and try again'));
    }
  }

  /**
   * AJAX action that attempts to add a like to a given Best Practice Model (identified by id)
   * @param integer $id the ID of the model to which the comment belongs
   */
  public function actionAddKarma($id) {
    
    if (!YII_DEBUG && !Yii::app()->request->isAjaxRequest) {
      throw new CHttpException('403', 'Forbidden access.');
    }
	  	  
    //JSON Headers
    header('Content-Type: application/json; charset="UTF-8"');

    //Allow only valid opinion parameters
    if (!isset($_GET['opinion']) || ($_GET['opinion'] !== "LIKE" &&
				     $_GET['opinion'] !== "DISLIKE")) {
      echo json_encode(array('status'=>'error',
			     'message'=>'Invalid Request.'));
      return;
    }
				
    //Load the new opinion, bestpractice, karma and karmarecord models
    $new_opinion = $_GET['opinion'];
    $is_new = false;
    $bestpractice = $this->loadModel($id);
    $karma = $bestpractice->getKarma();	
    //Lookup karma record if it exists (there should only be one -- database constraint should specify unique user IDs)
    $karmarecord = KarmaRecord::model()->findByAttributes(array('fk_type_id'=>$bestpractice->getPrimaryKey(),'user_id'=>Yii::app()->user->id, 'type'=>$bestpractice->tableName()));
    //If the karma record could not be found, create a new one
    if ($karmarecord === NULL){
      $is_new = true;
      $karmarecord = new KarmaRecord;
      $karmarecord->attributes = array('type'=>$bestpractice->tableName(),
				       'fk_type_id'=>$bestpractice->getPrimaryKey(),
				       'user_id'=>Yii::app()->user->id,
				       'opinion'=>$new_opinion);    
    }

    //If new opinion is NOT the same as old opinion, need to alter
    if ($karmarecord->opinion !== $new_opinion) {
      if ($karmarecord->opinion === 'LIKE') {//Changing from like to dislike
	$karma->likes--;
	$karma->dislikes++;
      } elseif ($karmarecord->opinion === 'DISLIKE') { //Changing from dislike to like
	$karma->likes++;
	$karma->dislikes--;
      }
      $karmarecord->opinion = $new_opinion; //Set the updated opinion
    } else {
      //Update karma if the karma record is new
      if ($is_new) {
	if ($new_opinion === 'LIKE')
	  $karma->likes++;
	else
	  $karma->dislikes++;
      }
    }
									 
    if ($karmarecord->save() && $karma->save()){
      echo json_encode(array('status'=>'success',
			     'message'=>'Thanks for telling us what you think!'));
							   
    } else {
      echo json_encode(array('status'=>'error',
			     'message'=>'There was an error adding your feedback... Please try again later'));
    }
	
	
  }



  /**
   * Returns the data model based on the primary key given in the GET variable.
   * If the data model is not found, an HTTP exception will be raised.
   * @param integer the ID of the model to be loaded
   */
  public function loadModel($id)
  {
    $model=ContextSuggestion::model()->findByPk($id);
    if($model===null)
      throw new CHttpException('404','The requested page does not exist.');
    return $model;
  }


  // -----------------------------------------------------------
  // Uncomment the following methods and override them if needed
  /*
    public function filters()
    {
    // return the filter configuration for this controller, e.g.:
    return array(
    'inlineFilterName',
    array(
    'class'=>'path.to.FilterClass',
    'propertyName'=>'propertyValue',
    ),
    );
    }

    public function actions()
    {
    // return external action classes, e.g.:
    return array(
    'action1'=>'path.to.ActionClass',
    'action2'=>array(
    'class'=>'path.to.AnotherActionClass',
    'propertyName'=>'propertyValue',
    ),
    );
    }
  */
}
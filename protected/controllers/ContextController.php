<?php

class ContextController extends Controller
{

  //Some constants for JSON responses	
  private $AJAX_SUGGEST_INVALID_CAPTCHA = 'Invalid reCaptcha entry -- Please re-enter the Captcha and try again';
  private $AJAX_SUGGEST_INVALID_MODEL = 'Your Context suggestion could not be submitted... Please ensure that all required fields are filled and try again.';
  private $AJAX_SUGGEST_SUCCESS = 'Context suggestion submitted! Thanks for contributing to Konvention.';


  public function filters()
  {
    // return the filter configuration for this controller, e.g.:
    return array('accessControl');
  }

  /**
   * Specifies the access control rules.
   * This method is used by the 'accessControl' filter.
   * @return array access control rules
   */
  public function accessRules()
  {
    return array(
		 array('allow',  // allow all users to perform 'index' and 'view' actions
		       'actions'=>array('index','view','viewbyname','list'),
		       'users'=>array('*'),
		       ),
		 array('allow', //Allow only authenticated users to update 
		       'actions'=>array('create','suggestnew','viewbyname','list'),
		       'users'=>array('@'),
		       ),
		 array('allow', // allow admin user do anything they feel like.		       
		       'users'=>array('vados'),
		       ),
		 array('deny',  // deny all users
		       'users'=>array('*'),
		       ),
		 );
  }



  /*    public function actions()
	{
	// return external action classes, e.g.:
	return array(
	'action1'=>'path.to.ActionClass',
	'action2'=>array(
	'class'=>'path.to.AnotherActionClass',
	'propertyName'=>'propertyValue',
	),
	);
	}
  */

  /*
   * Show list of contexts available
   */
  public function actionIndex()
  {
    $this->render('index');
  }
      
  /*
   * Produce list of contexts on ajax request (expecting insertion into datatables data)
   */
  public function actionList(){
    //If JSON request, produce listing, else produce html
    if (!YII_DEBUG && !Yii::app()->request->isAjaxRequest) {
      throw new CHttpException('403', 'Forbidden access.');	  
    } else {
      //JSON Headers
      header('Content-Type: application/json; charset="UTF-8"');    
	  
      $context_list = Context::model()->findAll(array('order'=>'name'));
      $result = array('aaData'=>array(), 'aaColumns'=>array(),'aaSorting'=>array());
	  
      //Add data
      foreach ($context_list as $context){
	array_push($result['aaData'], array($context->name,
					    nl2br($context->description),
					    $context->ref_url));
      }
	  
      echo json_encode($result);
    }
  }
      
 
  /*
   * Show form for creating new context (AJAX Submission)
   */ 
  public function actionCreate()
  {
    //Import the recaptcha lib
    Yii::import('ext.recaptcha.recaptchalib', true);

    //Public key from recaptcha
    $publickey = "6LcoRdkSAAAAAAxbwbugtOI1mtXBtdBnaRqI4OOJ"; // you got this from the signup page    
    $this->render('create', array('recaptcha_html'=>recaptcha_get_html($publickey)));	  
  }

  /**
   * Ajax submission of suggestions for new contexts
   */
  public function actionSuggestNew()
  {
    //Import the recaptcha lib
    Yii::import('ext.recaptcha.recaptchalib', true);

    if (!YII_DEBUG && !Yii::app()->request->isAjaxRequest) {
      throw new CHttpException('403', 'Forbidden access.');
    }
	  	  
    //JSON Headers
    header('Content-Type: application/json; charset="UTF-8"');    

    //Ensure that form_data (containing model data) and recaptcha are specified
    if (isset($_POST['form_data']) && 	
	isset($_POST['recaptcha_challenge_field']) && 
	isset($_POST['recaptcha_response_field'])){
      
      //Get the form data into an array & extract the array-index model data
      $form_data = array();
      parse_str($_POST['form_data'],$form_data);
      $context_data = ((isset($form_data['ContextSuggestion'])) ? $form_data['ContextSuggestion'] : null);
      
      //Captcha
      $privatekey = "6LcoRdkSAAAAAJFfPYBtDdo_x0kaqXy_Ei0ymjRC";
      $captcha_resp = recaptcha_check_answer ($privatekey,
					      $_SERVER["REMOTE_ADDR"],
					      $_POST["recaptcha_challenge_field"],
					      $_POST["recaptcha_response_field"]);
      
      //Create a karma object for this best practice
      $karma = new Karma;
      $karma->attributes = array('type'=> "ContextSuggestion"); 
      $karma->save();

      //Insert model data for validation
      $model = new ContextSuggestion;
      $context_data['fk_karma_id'] = $karma->karma_id;
      $model->attributes = $context_data;

      //Present JSON output, dependent on captcha and validation/save
      if ($captcha_resp->is_valid && $model->save()){

	//Record an activity for the new comment
	$activityrecord = new ActivityRecord;
	$new_url = Yii::app()->createUrl(strtolower($model->tableName()) . '/view/' . $model->name);
	$activityrecord->attributes = array('username'=>Yii::app()->user->name,
					    'desc'=>'suggested a new context',
					    'fk_type'=>$model->tableName(),
					    'fk_type_id'=>$model->contextsuggestion_id,
					    'url'=> $new_url,
					    'subject'=>$model->name);
	$activityrecord->save();  

	echo json_encode(array('status'=>'success', 'message'=> $this->AJAX_SUGGEST_SUCCESS));	

      } else {
    	echo json_encode(array('status'=>'error', 'message'=> ($captcha_resp->is_valid) ? $this->AJAX_SUGGEST_INVALID_MODEL : $this->AJAX_SUGGEST_INVALID_CAPTCHA, 'debug'=>print_r($model->getErrors(),true)));
      }
      
    } else {
      //In the case that correct form data has not been submitted
      echo json_encode(array('status'=>'error', 'message'=> $this->AJAX_SUGGEST_INVALID_MODEL));
    }

  }
    

  /**
   * Displays a particular model.
   * @param integer $id the ID of the model to be displayed
   */
  public function actionView($id)
  {
    $model = $this->loadModel($id);
    $this->render('view',array(
			       'model'=>$model,
			       'best_practice_list'=>$this->getBestPracticeList($model->context_id),
			       ));
  }

  /**
   * Look up and display a context's data by context name
   * @param string $contextname The contextname of the Context to be retrieved
   */
  public function actionViewByName($contextname)
  {
    $model = $this->loadByName($contextname);
    $this->render('view',array(
			       'model'=>$model,
			       'best_practice_list'=>$this->getBestPracticeList($model->context_id),
			       ));
  }


  /**
   * Returns the Context based on the primary key given in the GET variable.
   * If the data model is not found, an HTTP exception will be raised.
   * @param integer contextname The contextname of the model to be loaded
   */
  public function loadByName($contextname)
  {
    $model=Context::model()->findByAttributes(array('name'=>$contextname));
    if($model===null)
      throw new CHttpException('404','The requested page does not exist.');
    return $model;
  }



  /*
   * Get list of best practices belonging to a given context (found by ID)
   */
  public function getBestPracticeList($context_id)
  {
    return BestPractice::model()->findAllByAttributes(array('context_id'=>$context_id));
  }

  /**
   * Returns the data model based on the primary key given in the GET variable.
   * If the data model is not found, an HTTP exception will be raised.
   * @param integer the ID of the model to be loaded
   */
  public function loadModel($id)
  {
    $model=Context::model()->findByPk($id);
    if($model===null)
      throw new CHttpException('404','The requested page does not exist.');
    return $model;
  }

}
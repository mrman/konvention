<?php

class BestpracticeController extends Controller
{

  public function filters()
  {
    return array( 'accessControl' );
  }

  public function accessRules()
  {
    return array(
		 array('allow', // allow anonymous users to perform 'index' and 'view' actions
		       'actions'=>array('index','view','viewbyname','list','getkarma'),
		       'users'=>array('?'),
		       ),
		 array('allow', //Allow only authenticated users to create and comment 
		       'actions'=>array('index','view','viewbyname','create','submitcomment','addkarma','getkarma','list'),
		       'users'=>array('@'),
		       ),
		 array('allow', // allow admin user do anything they feel like.
		       'users'=>array('vados'),
		       ),
		 array('deny',  // deny all users
		       'users'=>array('*'),
		       ),
		 );
  }

  public function actionIndex()
  {
    $this->render('index');
  }

  /*
   * Produce list of best practices on ajax request (expecting insertion into datatables data)
   */
  public function actionList($context=''){
    //If JSON request, produce listing, else produce html
    if (!YII_DEBUG && !Yii::app()->request->isAjaxRequest) {
      throw new CHttpException('403', 'Forbidden access.');	  
    } else {
      //JSON Headers
      header('Content-Type: application/json; charset="UTF-8"');    
	  
      //Determine context to search for, if there is one
      //Look in the BestPractice+Karma listing, which is a joined to show likes
      $best_practice_list = array();
      if ($context === ''){
	$best_practice_list = BestPracticeKarmaView::model()->findAll();
      } else {
	//Look up the context provided
	$context_obj = Context::model()->findByAttributes(array('name'=>$context));
	if ($context_obj === NULL){ //If the context isn't found, send json error and exit
	  echo json_encode(array('status'=>'error','message'=>'Could not find specified context'));
	  return;
	}
	//Fill up the result array with the specified context
	$best_practice_list = BestPracticeKarmaView::model()->findAllByAttributes(array('context_id'=>$context_obj->context_id));
      }
      
      $result = array('aaData'=>array(), 'aaColumns'=>array(),'aaSorting'=>array());
	  
      //Add data
      foreach ($best_practice_list as $best_practice){
	array_push($result['aaData'], array($best_practice->name,
					    $best_practice->context->name,
					    $best_practice->description,
					    $best_practice->ref_url,
					    $best_practice->likes));
	  }
      
      echo json_encode($result);
    }
  }


  /**
   * Displays a particular model.
   * @param integer $id the ID of the model to be displayed
   */
  public function actionView($id)
  {
    $model = $this->loadModel($id);
    $this->render('view',array('model'=>$model));
  }

  /**
   * Look up and display a Best Practice's data by context name
   * @param string $bestpracticename The contextname of the Context to be retrieved
   */
  public function actionViewByName($contextname,$bestpracticename)
  {
    $model = $this->loadByName($contextname,$bestpracticename);
    $this->render('view',array('model'=>$model));
  }

  /**
   * Returns the Best Practice based on the primary key given in the GET variable.
   * If the data model is not found, an HTTP exception will be raised.
   * @param integer best practice name The best practice name of the model to be loaded
   */
  public function loadByName($contextname,$bestpracticename)
  {
    //Find related context
    $context=Context::model()->findByAttributes(array('name'=>$contextname));
    if($context===null)
      throw new CHttpException('404',"The requested page does not exist, could not find specified context: $contextname");

    //Find the best practice it belongs to
    $model=BestPractice::model()->findByAttributes(array('name'=>$bestpracticename, 'context_id'=>$context->context_id));
    if($model===null)
      throw new CHttpException('404',"The requested page does not exist, could not find specified bestpractice: \"$bestpracticename\"");
    return $model;
  }

  //Expecting name of a context that will be associated with the Best Practice that is to be created
  public function actionCreate($context="")
  {
    $model = new BestPractice;
	  
    //Save new best practice if self-submit
    if (isset($_POST['BestPractice']) && isset($_POST['context'])){
	  
      //Find the context related to this best practice
      $context = Context::model()->find("name=?",array($_POST['context']));
		  
      
      //Create a karma object for this best practice
      $karma = new Karma;
      $karma->attributes = array('type'=> $model->tableName()); 
      $karma->save();
      
      //Update the potential model -- note that context id must be transformed first before being put in	  
      $attribs = $_POST['BestPractice'];
      $attribs['context_id'] = $context->context_id;
      $attribs['fk_karma_id'] = $karma->karma_id;
      $model->attributes= $attribs;
      
      //Attempt to save the model and associated karma object
      if ($model->save()){
	
	//Update the karma object's fk_type_id
	$karma->fk_type_id = $model->bestpractice_id;
	$karma->update();
	
	//Record the creation of the bestpractice
	$activityrecord = new ActivityRecord;
	$new_url = Yii::app()->createUrl('context/' . $context->name . '/bestpractice/' . $model->name);
	$activityrecord->attributes = array('username'=>Yii::app()->user->name,
					    'desc'=>'added a best practice',
					    'fk_type'=>$model->tableName(),
					    'fk_type_id'=>$model->bestpractice_id,
					    'url'=> $new_url,
					    'subject'=>$model->name);
	$activityrecord->save();  
	
	//Redirect the user to the view of the new model
	$this->redirect(array('view','id'=>$model->bestpractice_id));
	
      }  else {
	$this->render('create',array('context'=>$context, 'error'=>"Could not create new Best Practice"));      	
      } 
    }
    
    //Display form if not self-submit
    $this->render('create', array('context'=>$context));
  }
			
  /*    public function actions()
	{
	// return external action classes, e.g.:
	return array(
	'action1'=>'path.to.ActionClass',
	'action2'=>array(
	'class'=>'path.to.AnotherActionClass',
	'propertyName'=>'propertyValue',
	),
	);
	}
  */

  /**
   * Returns the data model based on the primary key given in the GET variable.
   * If the data model is not found, an HTTP exception will be raised.
   * @param integer $id the ID of the model to be loaded
   */
  private function loadModel($id)
  {
    $model=BestPractice::model()->findByPk($id);
    if($model===null)
      throw new CHttpException('404','The requested page does not exist.');
    return $model;
  }


  /**
   * AJAX action that attempts to add a comment to a given Best Practice Model (identified by id)
   * @param integer $id the ID of the model to which the comment belongs
   */
  public function actionSubmitComment($id) {
    
    if (!YII_DEBUG && !Yii::app()->request->isAjaxRequest) {
      throw new CHttpException('403', 'Forbidden access.');
    }
	  	  
    //JSON Headers
    header('Content-Type: application/json; charset="UTF-8"');

    //Create a new comment and populate it with http headers
    $new_comment = new Comment;
    if (isset($_POST['comment'])) {
      $new_comment->username = Yii::app()->user->name;
      $new_comment->type = "BestPractice";
      $new_comment->fk_type_id = $id;
      $new_comment->comment = $_POST['comment']; //This needs to be escaped
    }

    //	$debug = print_r($new_comment->attributes, true);
      
    //Attempt to save the new comment
    if ($new_comment->save()){     
      echo json_encode(array('status'=>'success',
			     'message'=> 'Comment Saved',
			     'comment'=>array('username'=>$new_comment->username,
					      'submitted_at'=>$new_comment->submitted_at,
					      'text'=>$new_comment->comment)));
    } else {
      echo json_encode(array('status'=>'error',
			     //'debug'=>print_r($new_comment->getErrors(),true),
			     'message'=> 'Uh-oh, it looks like your comment could not be saved... Please wait a while and try again'));
    }
  }


  /** 
   * AJAX action to retrieve karma of requested best practice
   * @param integer $id The ID of the best practice
   */
  public function actionGetKarma($id) {
    if (!YII_DEBUG && !Yii::app()->request->isAjaxRequest) {
      throw new CHttpException('403', 'Forbidden access.');
    }
	  	  
    //JSON Headers
    header('Content-Type: application/json; charset="UTF-8"');

    $bestpractice = $this->loadModel($id);
    $karma = $bestpractice->getKarma();	
    $opinion = Yii::app()->user->isGuest ? '' : KarmaRecord::model()->findByAttributes(array('type'=> 'bestpractice',
											     'fk_type_id' => $bestpractice->bestpractice_id,
											     'user_id'=> Yii::app()->user->id))->opinion;
    
    echo json_encode(array('karma'=>$karma->total_karma(),
			   'likes'=> (int) $karma->likes,
			   'dislikes'=> (int) $karma->dislikes,
			   'opinion'=> strtolower($opinion)));
    return;
    
  }

  /**
   * AJAX action that attempts to add a like to a given Best Practice Model (identified by id)
   * @param integer $id the ID of the model to which the comment belongs
   */
  public function actionAddKarma($id) {
    
    if (!YII_DEBUG && !Yii::app()->request->isAjaxRequest) {
      throw new CHttpException('403', 'Forbidden access.');
    }
	  	  
    //JSON Headers
    header('Content-Type: application/json; charset="UTF-8"');

    //Allow only valid opinion parameters
    if (!isset($_GET['opinion']) || ($_GET['opinion'] !== "LIKE" &&
				     $_GET['opinion'] !== "DISLIKE")) {
      echo json_encode(array('status'=>'error',
			     'message'=>'Invalid Request.'));
      return;
    }
				
    //Load the new opinion, bestpractice, karma and karmarecord models
    $new_opinion = $_GET['opinion'];
    $is_new = false;
    $bestpractice = $this->loadModel($id);
    $karma = $bestpractice->getKarma();	
    //Lookup karma record if it exists (there should only be one -- database constraint should specify unique user IDs)
    $karmarecord = KarmaRecord::model()->findByAttributes(array('fk_type_id'=>$bestpractice->bestpractice_id,
								'user_id'=>Yii::app()->user->id, 
								'type'=>$bestpractice->tableName()));

    //If the karma record could not be found, create a new one
    if ($karmarecord === NULL){
      $is_new = true;
      $karmarecord = new KarmaRecord;
      $karmarecord->attributes = array('type'=>$bestpractice->tableName(),
				       'fk_type_id'=>$bestpractice->bestpractice_id,
				       'user_id'=>Yii::app()->user->id,
				       'opinion'=>$new_opinion);    
    }

    //If new opinion is NOT the same as old opinion, need to alter
    if ($karmarecord->opinion !== $new_opinion) {
      if ($new_opinion === 'LIKE') {//Changing from like to dislike
	$karma->likes++;
	$karma->dislikes--;
      } elseif ($new_opinion === 'DISLIKE') { //Changing from dislike to like
	$karma->dislikes++;
	$karma->likes--;
      }
      $karmarecord->opinion = $new_opinion; //Set the updated opinion
    } else {
      //Update karma if the karma record is new
      if ($is_new) {
	if ($new_opinion === 'LIKE')
	  $karma->likes++;
	else
	  $karma->dislikes++;
      }
    }

    if ($karmarecord->save() && $karma->save()){
      echo json_encode(array('status'=>'success',
			     'message'=>'Thanks for telling us what you think! Your opinion is being saved and will update shortly.'));
							   
    } else {
      echo json_encode(array('status'=>'error',
			     'message'=>'There was an error adding your feedback... Please try again later'));
    }
	
	
  }

}
<?php

class UserController extends Controller
{
  public $layout='//layouts/main';

  /**
   * @return array action filters
   */
  public function filters()
  {
    return array(
		 'accessControl', // perform access control for CRUD operations
		 'postOnly + delete', // we only allow deletion via POST request
		 );
  }

  /**
   * Specifies the access control rules.
   * This method is used by the 'accessControl' filter.
   * @return array access control rules
   */
  public function accessRules()
  {
    return array(
		 /*array('allow',  // allow all users to perform 'index' and 'view' actions
		   'actions'=>array('index','view'),
		   'users'=>array('*'),
		   ),*/
		 array('allow', // allow anonymous users to perform 'create' and 'view' actions
		       'actions'=>array('create','view','viewbyname'),
		       'users'=>array('?'),
		       ),
		 array('allow', //Allow only authenticated users to update 
		       'actions'=>array('update','view','viewbyname'),
		       'users'=>array('@'),
		       ),
		 array('allow', // allow admin user do anything they feel like.
		       'users'=>array('vados'),
		       ),
		 array('deny',  // deny all users
		       'users'=>array('*'),
		       ),
		 );
  }

  /**
   * Displays a particular model.
   * @param integer $id the ID of the model to be displayed
   */
  public function actionView($id)
  {
    $this->render('view',array(
			       'model'=>$this->loadUserById($id),
			       ));
  }

  /**
   * Look up and display a user's data by username
   * @param string $username The username of the User to be retrieved
   */
  public function actionViewByName($username)
  {
      $this->render('view',array(
				 'model'=>$this->loadUserByName($username),
				 ));
  }

  /**
   * Creates a new model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   */
  public function actionCreate()
  {

    //Disable user creation -- redirect to beta signup
    $this->redirect(array('site/betasignup'));

    $model=new User;
    $userinfo=new UserInfo;

    // Uncomment the following line if AJAX validation is needed
    // $this->performAjaxValidation($model);

    if(isset($_POST['User']) && isset($_POST['UserInfo']))
      {
	$model->attributes=$_POST['User'];
	$userinfo->attributes=$_POST['UserInfo'];

	$newrecord = $model->isNewRecord;
	
	//Hash the password using phpass before storing it
	$hasher = new PasswordHash(8,FALSE);	
	$model->password = $hasher->HashPassword($model->password);

	if($model->save()){
	  //Save the userinfo with the model id
	  $userinfo->user_id = $model->user_id;
	  $userinfo->save();

	  $this->redirect(array('site/login','new'=>true)); //Redirect to login so they can log in
	} else {
	  $model->password = '';
	}
      }

    $this->render('create',array(
				 'model'=>$model,
				 'userinfo'=>$userinfo,
				 ));
  }

  /**
   * Updates a particular model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id the ID of the model to be updated
   */
  public function actionUpdate($id)
  {

    //Error if non-admin user tries to update someone else's info
    if (Yii::app()->user->name !== 'vados' && Yii::app()->user->id !== $id){
      throw new CHttpException('401', 'Access Denied');
    }

    //Load the user & userinfo models
    $model=$this->loadUserById($id);
    $userinfo = UserInfo::model()->findByPk($id);

    // Uncomment the following line if AJAX validation is needed
    // $this->performAjaxValidation($model);

    if(isset($_POST['User']) && isset($_POST['UserInfo']))
      {
	//Update the user model
	$model->attributes=$_POST['User'];

	//Update the user info model
	$userinfo->attributes=$_POST['UserInfo'];

	if($model->save() && $userinfo->save())
	  $this->redirect(array('viewbyname','username'=>$model->username));
      }

    $this->render('update',array(
				 'model'=>$model,
				 'userinfo'=>$userinfo,
				 ));
  }

  /**
   * Deletes a particular model.
   * If deletion is successful, the browser will be redirected to the 'admin' page.
   * @param integer $id the ID of the model to be deleted
   */
  public function actionDelete($id)
  {
    $this->loadUserById($id)->delete();

    // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
    if(!isset($_GET['ajax']))
      $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('vados'));
  }

  /**
   * Lists all models.
   */
  public function actionIndex()
  {
    $dataProvider=new CActiveDataProvider('User');
    $this->render('index',array(
				'dataProvider'=>$dataProvider,
				));
  }

  /**
   * Manages all models.
   */
  public function actionAdmin()
  {
    $model=new User('search');
    $model->unsetAttributes();  // clear any default values
    if(isset($_GET['User']))
      $model->attributes=$_GET['User'];

    $this->render('admin',array(
				'model'=>$model,
				));
  }

  /**
   * Returns the User based on the primary key given in the GET variable.
   * If the data model is not found, an HTTP exception will be raised.
   * @param integer id The ID of the model to be loaded
   */
  public function loadUserById($id)
  {
    $model=User::model()->findByPk($id);
    if($model===null)
      throw new CHttpException('404','The requested page does not exist.');
    return $model;
  }

  /**
   * Returns the User based on the primary key given in the GET variable.
   * If the data model is not found, an HTTP exception will be raised.
   * @param integer username The username of the model to be loaded
   */
  public function loadUserByName($username)
  {
    $model=User::model()->findByAttributes(array('username'=>$username));
    if($model===null)
      throw new CHttpException('404','The requested page does not exist.');
    return $model;
  }

  /**
   * Performs the AJAX validation.
   * @param CModel the model to be validated
   */
  protected function performAjaxValidation($model)
  {
    if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
      {
	echo CActiveForm::validate($model);
	Yii::app()->end();
      }
  }
}

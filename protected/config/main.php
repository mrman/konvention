<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
Yii::setPathOfAlias('phpass', Yii::getPathOfAlias('application.extensions.phpass')); //Create alias for PHPass password hashing plugin

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	     'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	     'name'=>'Konvention',

	     // preloading 'log' component
	     'preload'=>array('log'),
	     // autoloading model and component classes
	     'import'=>array(
			     'application.models.*',
			     'application.components.*',
			     'application.extensions.phpass.PasswordHash', //Load phpass
			     ),

	     /*	     'modules'=>array(
			      // uncomment the following to enable the Gii tool
			      'gii'=>array(
				'class'=>'system.gii.GiiModule',
				'password'=>'password12',
				// If removed, Gii defaults to localhost only. Edit carefully to taste.
				'ipFilters'=>array('127.0.0.1','::1', '192.168.0.190'),						  ),
			 
				),*/

			      // application components
			      'components'=>array(
						  'cache'=>array(
								 'class'=>'CDbCache',
								 ),
						  'db'=>array(
							      'class'=>'system.db.CDbConnection',
							      'connectionString'=>'sqlite:/public_html/konvention/protected/data/konvention.db',
							      'schemaCachingDuration'=>3600,
							      ),
						  'mailer'=>array(
								  'class' => 'application.extensions.mailer.EMailer',
								  'pathViews' => 'application.views.email',
								  'pathLayouts' => 'application.views.email.layouts'
								  ),
	     		 
						  // Disable built in jquery lib
						  'clientScript'=>array(
									'class' => 'CClientScript',
									'scriptMap' => array(
											     'jquery.js'=>false,
											     ),
									'coreScriptPosition' => CClientScript::POS_BEGIN,
									),

						  //Cookie-based Auth
						  'user'=>array(
								// enable cookie-based authentication
								'allowAutoLogin'=>true,
								),
						  // uncomment the following to enable URLs in path-format
						  'urlManager'=>array(
								      'urlFormat'=>'path',
								      'rules'=>array(
										     '<controller:\w+>/<id:\d+>'=>'<controller>/view',
										     '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
										     '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
										     //User
										     'user/view/<username:.+>'=> 'user/viewbyname', //View by name
										     //Context
										     'context/view/<contextname:.+>'=> 'context/viewbyname', //View by name
										     //ContextSuggestion
										     'contextsuggestion/view/<contextsuggestionname:.+>'=> 'contextsuggestion/viewbyname', //View by name
										     //Best Practice										     
										     'context/<contextname:.+>/bestpractice/<bestpracticename:.+>'=> 'bestpractice/viewbyname', //View by name
										     ),
								      'showScriptName'=>false,
								      'caseSensitive'=>false,
								      ),
						  //		'db'=>array(
						  //'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
						  //),
						  // uncomment the following to use a MySQL database	
						  'db'=>array(
							      'connectionString' => 'mysql:host=localhost;dbname=vados_konvention',//'mysql:host=192.168.0.196;dbname=konvention_dev',
							      'emulatePrepare' => true,
							      'username' => 'vados_konquery',
							      'password' => 'password12',
							      'charset' => 'utf8',
							      ),
						  'errorHandler'=>array(
									// use 'site/error' action to display errors
									'errorAction'=>'site/error',
									),
						  'log'=>array(
							       'class'=>'CLogRouter',
							       'routes'=>array(
									       array(
										     'class'=>'CFileLogRoute',
										     'levels'=>'error, warning',
										     ),
									       // uncomment the following to show log messages on web pages
									       /*
										 array(
										 'class'=>'CWebLogRoute',
										 ),
									       */
									       ),
							       ),
						  ),

			      // application-level parameters that can be accessed
			      // using Yii::app()->params['paramName']
			      'params'=>array(
					      // this is used in contact page
					      'adminEmail'=>'admin@vadosware.com',
					      'adminName'=>'Victor Adossi',
					      ),
			      );
<?php

/**
 * This is the model class for table "ActivityRecord".
 *
 * The followings are the available columns in table 'ActivityRecord':
 * @property string $username
 * @property string $desc
 * @property string $fk_type
 * @property integer $fk_type_id
 * @property string $timeof
 * @property string $url
 * @property string $subject
 */
class ActivityRecord extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return ActivityRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ActivityRecord';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, desc, fk_type, fk_type_id, url, subject', 'required'),
			array('fk_type_id', 'numerical', 'integerOnly'=>true),
			array('username, desc, fk_type, url, subject', 'length', 'max'=>128),
			array('timeof', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('username, desc, fk_type, fk_type_id, timeof, url, subject', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'username' => 'Username',
			'desc' => 'Desc',
			'fk_type' => 'Fk Type',
			'fk_type_id' => 'Fk Type',
			'timeof' => 'Timeof',
			'url' => 'Url',
			'subject' => 'Subject',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('username',$this->username,true);

		$criteria->compare('desc',$this->desc,true);

		$criteria->compare('fk_type',$this->fk_type,true);

		$criteria->compare('fk_type_id',$this->fk_type_id);

		$criteria->compare('timeof',$this->timeof,true);

		$criteria->compare('url',$this->url,true);

		$criteria->compare('subject',$this->subject,true);

		return new CActiveDataProvider('ActivityRecord', array(
			'criteria'=>$criteria,
		));
	}
}
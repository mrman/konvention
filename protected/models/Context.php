<?php

  /**
   * This is the model class for table "Context".
   *
   * The followings are the available columns in table 'Context':
   * @property integer $convention_id
   * @property string $name
   * @property string $description
   */
class Context extends CActiveRecord
{

  /*
   *Updates the Context information with information for a given ContextSuggestion (used in promotion of a ContextSuggestion)
   *@param ContextSuggestion $cs The Context Suggestion to utilize in creating the new Context
   *@return boolean success/failure
   */
  public function slurpSuggestion($cs){
    if (!is_null($cs) && $cs instanceof ContextSuggestion){
      $this->name = $cs->name;
      $this->description = $cs->description;
      $this->ref_url = $cs->ref_url;
      return true;
    } else {
      return false;
    }
  }

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return Context the static model class
   */
  public static function model($className=__CLASS__)
  {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName()
  {
    return 'Context';
  }

  /**
   * @return string that represents the logical name of the model
   */
  public function semanticName()
  {
    return 'Context';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules()
  {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
		 array('name, description, ref_url', 'required'),
		 array('name', 'length', 'max'=>128),
		 // The following rule is used by search().
		 // Please remove those attributes that should not be searched.
		 array('convention_id, name, description', 'safe', 'on'=>'search'),
		 );
  }

  /**
   * @return array relational rules.
   */
  public function relations()
  {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array(
		);
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels()
  {
    return array(
		 'convention_id' => 'Convention',
		 'name' => 'Name',
		 'description' => 'Description',
		 'ref_url' => 'Reference URL',
		 );
  }

  /**
   * Retrieves a list of models based on the current search/filter conditions.
   * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
   */
  public function search($query,$context)
  {
    // Warning: Please modify the following code to remove attributes that
    // should not be searched.

    $criteria=new CDbCriteria;

    //Search query against name, with partial matching, and or the compares together
    $criteria->compare('name',$query,true,'OR');
    $criteria->compare('description',$query,true,'OR');
    //$criteria->compare('ref_url',$this->ref_url,true);

    //Adjust if user has added a context
    if ($context !== 'all'){
      $context_obj = Context::model()->findByAttributes(array('name'=>$context));
      //Ensure context model is found
      if (is_null($context_obj))
	throw new CHttpException('400','Invalid Request (Context is invalid)');
      $criteria->compare('context_id',$context_obj->context_id,false);
    }

    return new CActiveDataProvider($this, array(
						'criteria'=>$criteria,
						));
  }
}
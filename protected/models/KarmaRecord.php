<?php

/**
 * This is the model class for table "KarmaRecord".
 *
 * The followings are the available columns in table 'KarmaRecord':
 * @property integer $fk_type_id
 * @property string $type
 * @property string $user_id
 * @property string $opinion
 */
class KarmaRecord extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return KarmaRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'KarmaRecord';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fk_type_id, type, user_id, opinion', 'required'),
			array('fk_type_id', 'numerical', 'integerOnly'=>true),
			array('type', 'length', 'max'=>128),
			array('user_id', 'length', 'max'=>45),
			array('opinion', 'length', 'max'=>7),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('fk_type_id, type, user_id, opinion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'fk_type_id' => 'Fk Type',
			'type' => 'Type',
			'user_id' => 'User',
			'opinion' => 'Opinion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('fk_type_id',$this->fk_type_id);

		$criteria->compare('type',$this->type,true);

		$criteria->compare('user_id',$this->user_id,true);

		$criteria->compare('opinion',$this->opinion,true);

		return new CActiveDataProvider('KarmaRecord', array(
			'criteria'=>$criteria,
		));
	}
}
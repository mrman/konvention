<?php

/**
 * This is the model class for table "Comment".
 *
 * The followings are the available columns in table 'Comment':
 * @property integer $comment_id
 * @property string $type
 * @property integer $fk_type_id
 * @property string $username
 * @property string $modified_at
 * @property string $submitted_at
 * @property string $comment
 */
class Comment extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Comment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Comment';
	}

	/**
	 * @return string that represents the logical name of the model
	 */
	public function semanticName()
	{
		return 'Comment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('type, fk_type_id, username, comment', 'required'),
			array('fk_type_id', 'numerical', 'integerOnly'=>true),
			array('type, username', 'length', 'max'=>128),
			array('submitted_at', 'safe'),
			//Timestamp attributes
			array('modified_at','default',
			      'value'=>new CDbExpression('NOW()'),
			      'setOnEmpty'=>false,'on'=>'update'),
			array('submitted_at','default',
			      'value'=>new CDbExpression('NOW()'),
			      'setOnEmpty'=>false,'on'=>'insert'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('comment_id, type, fk_type_id, username, modified_at, submitted_at, comment', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'comment_id' => 'Comment',
			'type' => 'Type',
			'fk_type_id' => 'Fk Type',
			'username' => 'Username',
			'modified_at' => 'Modified At',
			'submitted_at' => 'Submitted At',
			'comment' => 'Comment',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('comment_id',$this->comment_id);

		$criteria->compare('type',$this->type,true);

		$criteria->compare('fk_type_id',$this->fk_type_id);

		$criteria->compare('username',$this->username,true);

		$criteria->compare('modified_at',$this->modified_at,true);

		$criteria->compare('submitted_at',$this->submitted_at,true);

		$criteria->compare('comment',$this->comment,true);

		return new CActiveDataProvider('Comment', array(
			'criteria'=>$criteria,
		));
	}
}
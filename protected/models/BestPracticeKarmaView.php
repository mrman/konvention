<?php

/**
 * This is the model class for table "BestPracticeKarmaView".
 *
 * The followings are the available columns in table 'BestPracticeKarmaView':
 * @property integer $bestpractice_id
 * @property string $name
 * @property string $description
 * @property string $ref_url
 * @property integer $context_id
 * @property integer $likes
 */
class BestPracticeKarmaView extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return BestPracticeKarmaView the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'BestPracticeKarmaView';
	}

	/**
	 * @return primary key
	 */
	public function primaryKey(){
	  return 'bestpractice_id';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, description, ref_url, context_id', 'required'),
			array('bestpractice_id, context_id, likes', 'numerical', 'integerOnly'=>true),
			array('name, ref_url', 'length', 'max'=>128),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('bestpractice_id, name, description, ref_url, context_id, likes', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			     'context' => array(self::BELONGS_TO, 'Context', 'context_id'),			     
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'bestpractice_id' => 'Bestpractice',
			'name' => 'Name',
			'description' => 'Description',
			'ref_url' => 'Ref Url',
			'context_id' => 'Context ID',
			'likes' => 'Likes',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('bestpractice_id',$this->bestpractice_id);

		$criteria->compare('name',$this->name,true);

		$criteria->compare('description',$this->description,true);

		$criteria->compare('ref_url',$this->ref_url,true);

		$criteria->compare('context_id',$this->context_id);

		$criteria->compare('likes',$this->likes);

		return new CActiveDataProvider('BestPracticeKarmaView', array(
			'criteria'=>$criteria,
		));
	}
}
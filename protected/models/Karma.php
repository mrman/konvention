<?php

/**
 * This is the model class for table "Karma".
 *
 * The followings are the available columns in table 'Karma':
 * @property integer $karma_id
 * @property string $type
 * @property string $fk_type_id
 * @property integer $likes
 * @property integer $dislikes
 */
class Karma extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Karma the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Karma';
	}

	/**
	 * @return integer Total karma (likes - dislikes)
	 */
	public function total_karma() {
	  return $this->likes - $this->dislikes;
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('type', 'required'),
			array('likes, dislikes', 'numerical', 'integerOnly'=>true),
			array('type, fk_type_id', 'length', 'max'=>128),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('karma_id, type, fk_type_id, likes, dislikes', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'karma_id' => 'Karma',
			'type' => 'Type',
			'fk_type_id' => 'Fk Type',
			'likes' => 'Likes',
			'dislikes' => 'Dislikes',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('karma_id',$this->karma_id);

		$criteria->compare('type',$this->type,true);

		$criteria->compare('fk_type_id',$this->fk_type_id,true);

		$criteria->compare('likes',$this->likes);

		$criteria->compare('dislikes',$this->dislikes);

		return new CActiveDataProvider('Karma', array(
			'criteria'=>$criteria,
		));
	}
}
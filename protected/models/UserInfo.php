<?php

/**
 * This is the model class for table "UserInfo".
 *
 * The followings are the available columns in table 'UserInfo':
 * @property integer $user_id
 * @property string $facebook_id
 * @property string $googleplus_id
 * @property string $github_id
 * @property string $twitter_id
 * @property string $tumblr_id
 */
class UserInfo extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return UserInfo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'UserInfo';
	}

	/**
	 * @return string the associated database table name
	 */
	public function semanticName()
	{
		return 'Context Suggestion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('facebook_id, googleplus_id, github_id, twitter_id, tumblr_id', 'length', 'max'=>128),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('user_id, facebook_id, googleplus_id, github_id, twitter_id, tumblr_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'facebook_id' => 'Facebook Url',
			'googleplus_id' => 'Googleplus Url',
			'github_id' => 'Github Url',
			'twitter_id' => 'Twitter Url',
			'tumblr_id' => 'Tumblr Url',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id);

		$criteria->compare('facebook_id',$this->facebook_id,true);

		$criteria->compare('googleplus_id',$this->googleplus_id,true);

		$criteria->compare('github_id',$this->github_id,true);

		$criteria->compare('twitter_id',$this->twitter_id,true);

		$criteria->compare('tumblr_id',$this->tumblr_id,true);

		return new CActiveDataProvider('UserInfo', array(
			'criteria'=>$criteria,
		));
	}
}
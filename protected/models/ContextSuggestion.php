<?php

/**
 * This is the model class for table "ContextSuggestion".
 *
 * The followings are the available columns in table 'ContextSuggestion':
 * @property integer $contextsuggestion_id
 * @property string $name
 * @property string $description
 * @property string $comment
 * @property string $ref_url
 * @property integer $fk_karma_id
 */
class ContextSuggestion extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return ContextSuggestion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ContextSuggestion';
	}

	/**
	 * @return string the associated database table name
	 */
	public function semanticName()
	{
		return 'Context Suggestion';
	}

	/**
	 * Lookup the Karma object related to this BestPractice
	 */
	public function getKarma(){
	  return Karma::model()->findByPk($this->fk_karma_id);	  
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, description, comment, ref_url, fk_karma_id', 'required'),
			array('fk_karma_id', 'numerical', 'integerOnly'=>true),
			array('name, ref_url', 'length', 'max'=>128),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('contextsuggestion_id, name, description, comment, ref_url, fk_karma_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'fk_karma' => array(self::BELONGS_TO, 'Karma', 'fk_karma_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'contextsuggestion_id' => 'Contextsuggestion',
			'name' => 'Name',
			'description' => 'Description',
			'comment' => 'Comment',
			'ref_url' => 'Ref Url',
			'fk_karma_id' => 'Fk Karma',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('contextsuggestion_id',$this->contextsuggestion_id);

		$criteria->compare('name',$this->name,true);

		$criteria->compare('description',$this->description,true);

		$criteria->compare('comment',$this->comment,true);

		$criteria->compare('ref_url',$this->ref_url,true);

		$criteria->compare('fk_karma_id',$this->fk_karma_id);

		return new CActiveDataProvider('ContextSuggestion', array(
			'criteria'=>$criteria,
		));
	}
}
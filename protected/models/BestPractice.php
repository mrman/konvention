<?php

/**
 * This is the model class for table "BestPractice".
 *
 * The followings are the available columns in table 'BestPractice':
 * @property integer $bestpractice_id
 * @property string $name
 * @property string $description
 * @property string $snippet
 * @property string $ref_url
 * @property integer $context_id
 */
class BestPractice extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return BestPractice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'BestPractice';
	}

	/**
	 * @return string that represents the logical name of the model
	 */
	public function semanticName()
	{
		return 'Best Practice';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, description, ref_url, context_id, fk_karma_id', 'required'),
			array('context_id', 'numerical', 'integerOnly'=>true),
			array('name, ref_url', 'length', 'max'=>128),
			array('snippet','safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('bestpractice_id, name, snippet, description, ref_url, context_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'context' => array(self::BELONGS_TO, 'Context', 'context_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'bestpractice_id' => 'Bestpractice',
			'name' => 'Name',
			'description' => 'Description',
			'snippet' => 'Code Snippet',
			'ref_url' => 'Ref Url',
			'context_id' => 'Context',
		);
	}

	/**
	 * Get the id for this Best Practice 
	 * @return integer The best practice id for the model
	 */
	public function id()
	{
	  return $this->bestpractice_id;
	}


	/**
	 * Lookup the Karma object related to this BestPractice
	 */
	public function getKarma(){
	  return Karma::model()->findByPk($this->fk_karma_id);	  
	}


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($query, $context)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$query_arr = preg_split("/[\s,.]+/",$query);

		//$criteria->compare('bestpractice_id',$this->bestpractice_id);		
		//Loop and add criteria for each area
		foreach ($query_arr as $query_str){
		$criteria->compare('name',$query_str,true,'OR');
		}
		foreach ($query_arr as $query_str){
		$criteria->compare('description',$query_str,true,'OR');
		}
		//$criteria->compare('name',$query_arr,true,'OR');
		//$criteria->compare('description',$query_arr,true,'OR');
		//$criteria->compare('snippet',$this->snippet,true);
		//$criteria->compare('ref_url',$this->ref_url,true);
		//$criteria->compare('context_id',$this->context_id);

		return new CActiveDataProvider('BestPractice', array(
			'criteria'=>$criteria,
		));
	}
}
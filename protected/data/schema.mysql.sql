CREATE TABLE User (
    user_id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(128) NOT NULL,
    password VARCHAR(128) NOT NULL,
    email VARCHAR(128) NOT NULL
);

CREATE TABLE Context (
    convention_id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL UNIQUE,
    description TEXT NOT NULL
);

INSERT INTO User (username, password, email) VALUES ('test1', 'pass1', 'test1@example.com');
INSERT INTO User (username, password, email) VALUES ('test2', 'pass2', 'test2@example.com');
INSERT INTO User (username, password, email) VALUES ('test3', 'pass3', 'test3@example.com');
INSERT INTO User (username, password, email) VALUES ('test4', 'pass4', 'test4@example.com');
INSERT INTO User (username, password, email) VALUES ('test5', 'pass5', 'test5@example.com');
INSERT INTO User (username, password, email) VALUES ('test6', 'pass6', 'test6@example.com');
INSERT INTO User (username, password, email) VALUES ('test7', 'pass7', 'test7@example.com');
INSERT INTO User (username, password, email) VALUES ('test8', 'pass8', 'test8@example.com');
INSERT INTO User (username, password, email) VALUES ('test9', 'pass9', 'test9@example.com');

INSERT INTO Context (name, description) VALUES ('php','PHP is a language written for the web.');
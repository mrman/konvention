/* Konvention Javascript code */

ALERT_TYPES = ['success','warning','error','info'];
UPDATE_INTERVAL = {karma: 5000};//Update interval in ms

$(document).ready(function() {

	//Enable Timeago jQuery plugin (both for abbr tags and the HTML5 standard time tag
	$("abbr.timeago").each(function(){$(this).timeago();});
	$("time.timeago").each(function(){$(this).timeago();});

	//Enable jTruncated fields
	if ($('.truncated')[0]){
	    $('.truncated').each(function(){
		    $(this).jTruncate({
			    length: 75,  
				minTrail: 0,  
				moreText: "[full]",  
				lessText: "[short]",  
				ellipsisText: "...",  
				moreAni: "fast",  
				lessAni: "fast",
				});
		});
	}

	//Perform UI Bindings
	bind_UI();

	//Activate Google Prettify
	prettyPrint();

	//Karma dropdown hover to show differential information
	$("#widget-karma > #differential").hover(
						 function(){ $("#differential-distribution").slideDown('fast')},
						 function(){ $("#differential-distribution").slideUp('fast')});
    });

//Custom UI element bindings
function bind_UI(){
    $(".slidedown-item > #slidedown-item-title").click(function() {
	    var content = $(this).parent().find("#slidedown-item-content");
	    var chevron = $(this).parent().find("#chevron");

	    //Exit if there is somehow no content (malformed slidedown)
	    if (content == null)
		return;

	    //Display/hide content, change chevron
	    if (content.is(":visible")){	    
		content.slideUp('fast');
		chevron.removeClass("icon-chevron-up");
		chevron.addClass("icon-chevron-down");
	    } else {
		content.slideDown('fast');
		chevron.removeClass("icon-chevron-down");
		chevron.addClass("icon-chevron-up");
	    }

	});

    //Table body hiding button
    $('.btnHideTableBody').click(function() {
	    tbody = $('#' + $(this).attr('data-target') + ' > tbody');
	    //Show/Hide the table body
	    tbody.toggle();
	    //Change the text
	    if (tbody.is(':visible'))
		$(this).html('Hide All');
	    else 
		$(this).html('Show All');
	});

    //Ajax Comment button
    $("#btnSubmitComment").click(function(e) {
	    var comment_list = $($(this).parent().siblings('#comment-list'));
	    $.ajax({
		    url: $("#btnSubmitComment").attr("href"),
			type: "POST",
			data: {
			comment: $("#comment-textarea").val(),
			    },
			}).done(function(data){
				if (data.status === "success"){	
				    $("#commentSubmissionResult").append("<div class=\"alert alert-success\">Comment successfully Submitted!</div>");

				    //Add the comment to the list
				    var comment_html = '';
				    comment_html += "\n<div class=\"comment\" id=\"comment-new\">\n";
				    comment_html += "<div class=\"comment-header\">\n";
				    comment_html += "<span id=\"username\"><a href=\"/user/view/" + data.comment.username + "\">" + data.comment.username + "</a></span>\n";
				    comment_html += " - Just posted";
				    comment_html += "</div>"; //End of header
				    comment_html += "<div class=\"comment-body\">\n";
				    comment_html += "<p id=\"comment-text\">" + data.comment.text + "</p>\n";
				    comment_html += "</div>\n";//End of comment body
				    comment_html += "</div>\n";
				    comment_list.append(comment_html);				    

				} else {
				    $("#commentSubmissionResult").append("<div class=\"alert alert-error\">Comment was not submitted... Please try again later</div>");
				}
			    });

	    e.preventDefault();
	});

    //Generic AJAX Form Submission Button code
    $(".ajaxSubmitButton").click(function(e){
	    e.preventDefault();
	
	    //Perform ajax request only if not disabled
	    if ($(this).hasClass("disabled") || $(this).is(":disabled") || $(this).attr('disabled'))
		return;	

	    //Save a reference to the button so that it can be disabled later
	    var clicked_btn = this;
	    //Get the form that contains this ajax button, quit & show error if one isn't found
	    var form = $(this).closest('form');
	    if (typeof form == 'undefined'){
		show_page_alert('error','A problem was encountered submitting your data... Please wait a while and try again.');
		return;
	    }
		
	    //Perform ajax submission of the Context Suggestion
	    $.ajax({
		    url: $(this).attr("href"),
			type: "POST",
			data: {
			form_data: form.serialize(),
			    //Recaptcha stuff 
			    recaptcha_challenge_field: $("#recaptcha_challenge_field").val(),
			    recaptcha_response_field: $("#recaptcha_response_field").val(),
			    }}).done(function(data){
				    show_page_alert(data.status,data.message);

				    //Disable the submission button (and apply styling) upon successful Submission
				    if (data.status === "success"){
					$(clicked_btn).addClass("disabled");
					$(clicked_btn).attr("disabled", "disabled");		    
				    }
				});

	});

    $(".ajaxSimpleButton").click(function(e){
	    e.preventDefault();
	
	    //Perform ajax request only if not disabled
	    if ($(this).hasClass("disabled") || $(this).is(":disabled") || $(this).attr('disabled'))
		return;	

	    //Save a reference to the button so that it can be disabled later
	    var clicked_btn = this;

	    //Perform ajax submission of the Context Suggestion
	    $.post($(this).attr("href"),function(data){
		    if ("status" in data && "message" in data)
			show_page_alert(data.status,data.message);
		});
	
	    //Disable the button if we're at the end
	    if ($(this).attr("data-doonce") == "true"){
		$(clicked_btn).addClass("disabled");
		$(clicked_btn).attr("disabled", "disabled");		    		    
	    }


	});


    //Generic AJAX Form Submission Button code
    $(".ajaxListExtender").click(function(e){
	    e.preventDefault();
	
	    //Perform ajax request only if not disabled
	    if ($(this).hasClass("disabled") || $(this).is(":disabled") || $(this).attr('disabled'))
		return;	

	    //Save a reference to the button so that it can be disabled later
	    var clicked_btn = this;

	    //Perform ajax to retrieve list items
	    $.ajax({
		    url: $(this).attr("href"),
			type: "POST",
			data: {
			'offset': $(this).attr('data-offset'),
			    'limit' : $(this).attr('data-limit'),
			    }}).done(function(data){
				    var table_id = $(clicked_btn).attr('data-list');
		
				    //Disable the extender button if this is the last
				    if (data.status === "success"){
					for (var i = 0; i < data.list.length; i++)
					    $("#" + table_id).append('<tr><td>' + data.list[i] + '</td></tr>');
		    
					//Reinitialize the timeago for the table
					$("#" + table_id + " time.timeago").timeago();

					//Disable the button if we're at the end
					if (data.finished){
					    $(clicked_btn).addClass("disabled");
					    $(clicked_btn).attr("disabled", "disabled");		    		    
					}

					//Update the offset
					var offset = parseInt($(clicked_btn).attr('data-offset'));
					$(clicked_btn).attr('data-offset', offset + data.list.length);
				    }
				});	
	});

    //Enable fields that need to be constantly updated
    if ($('.ajaxLiveField')[0]){
	var fields = $('.ajaxLiveField');

	for (var i=0; i < fields.length; i++){

	    var dom_obj = $(fields[i]);
	    var src = dom_obj.attr('data-src');
	    var field = dom_obj.attr('data-field');
	    setInterval(function() {
		    $.get(src).done(function(data){
			    //console.log('Updating field [' +  field + '], src = [' + src + ']');			    
			    if (field === 'karma')
				update_karma_widget(dom_obj,data);
			    else (!(typeof data[field] == 'undefined'))
				     dom_obj.html(data[field]);		    			    
			});

		}, UPDATE_INTERVAL[field]);//Check every two seconds
	}
    }

    //Search Results datatable init
    if ($('#search_results')[0]){
	$('#search_results').dataTable({
		"aaSorting": [[0, "asc"]],
		    "bProcessing": true});
    }


    //Context datatable init
    if ($('#context_list')[0]){
	$('#context_list').dataTable({
		"aaSorting": [[0, "asc"]],
		    "bProcessing": true,
		    "sAjaxSource": '/context/list',
		    "fnCreatedRow": function( nRow, aData, iDisplayIndex ) {
		    $('td:eq(1)',nRow).jTruncate({ 
			    length: 75,  
				minTrail: 0,  
				moreText: "[full]",  
				lessText: "[short]",  
				ellipsisText: "...",  
				moreAni: "fast",  
				lessAni: "fast",
				});
		    var contextName = $('td:eq(0)',nRow).html();
		    var url = $('td:eq(2)',nRow).html();
		    $('td:eq(0)',nRow).html('<a href="/context/view/' + contextName + '">' + contextName + '</a>');
		    $('td:eq(2)',nRow).html('<a href="' + url + '">' + url + '</a>');
		},
		    });
    }

    //Context Suggestion datatable init
    if ($('#suggested_context_list')[0]){
	$('#suggested_context_list').dataTable({
		"aaSorting": [[0, "asc"]],
		    "bProcessing": true,
		    "sAjaxSource": '/contextsuggestion/list',
		    "fnCreatedRow": function( nRow, aData, iDisplayIndex ) {
		    $('td:eq(1)',nRow).jTruncate({ 
			    length: 75,  
				minTrail: 0,  
				moreText: "[full]",  
				lessText: "[short]",  
				ellipsisText: "...",  
				moreAni: "fast",  
				lessAni: "fast",
				});
		    var contextName = $('td:eq(0)',nRow).html();
		    var url = $('td:eq(2)',nRow).html();
		    $('td:eq(0)',nRow).html('<a href="/contextsuggestion/view/' + contextName + '">' + contextName + '</a>');
		    $('td:eq(2)',nRow).html('<a href="' + url + '">' + url + '</a>');
		},
		    });
    }

    //Best Practice datatable init (belonging to a specific context)
    if ($('#bp_list')[0]){
	$('#bp_list').dataTable({
		"aaSorting": [[4, "desc"]],
		    "bProcessing": true,
		    "sAjaxSource": $('#bp_list').attr('data-src'),
		    "fnCreatedRow": function( nRow, aData, iDisplayIndex ) {
		    $('td:eq(2)',nRow).jTruncate({ //Used to be eq(1)
			    length: 75,  
				minTrail: 0,  
				moreText: "[full]",  
				lessText: "[short]",  
				ellipsisText: "...",  
				moreAni: "fast",  
				lessAni: "fast",
				});
		    var bpName = $('td:eq(0)',nRow).html();
		    var cName = $('td:eq(1)',nRow).html();
		    var url = $('td:eq(3)',nRow).html();
		    $('td:eq(0)',nRow).html('<a href="/context/' + cName + '/bestpractice/' + bpName + '">' + bpName + '</a>');
		    $('td:eq(3)',nRow).html('<a href="' + url + '">' + url + '</a>');		   
		},
		    });
    }
  

}

/* Functions to manage the alert section at the top of each page */
function show_page_alert(type,message){
    //Ensure type is valid
    if ($.inArray(type,window.ALERT_TYPES) < 0)
	return;

    //Hide the alert box, and load the correct classes 
    if ($("#alert-page").is(':visible'))
	$("#alert-page").hide();
    $("#alert-page").removeClass();
    $("#alert-page").addClass("alert alert-" + type);
    
    //Add message
    $("#alert-page > #message").html(message);

    //Show the alert box
    $("#alert-page").slideDown('fast');
}	


/* Functions to manage widgets on the page */
function update_karma_widget(total_span, karma_data){
    //Set the total count
    total_span.html(karma_data.karma);

    //Update the percentage bars on the widget
    bar_container = $(total_span.parent().parent().next('#differential-distribution'));
    success_bar = $(bar_container.find('.bar-success')[0]);
    danger_bar = $(bar_container.find('.bar-danger')[0]);

    //Update bar widths & text
    var total_votes = karma_data.likes + Math.abs(karma_data.dislikes);
    var like_percent = karma_data.likes / total_votes * 1.0;
    var dislike_percent = Math.abs(karma_data.dislikes) / total_votes * 1.0;
    if (karma_data.karma != 0){
	//Only need to make any changes if karma is not 0, since that is the default value
	success_bar.css('width',(like_percent * 100) + '%');
	danger_bar.css('width',(dislike_percent * 100) + '%');

	success_bar.html(karma_data.likes + ' like' + (karma_data.likes > 1 ? 's' : ''));
	danger_bar.html(karma_data.dislikes + ' dislike' + (karma_data.dislikes > 1 ? 's' : ''));
    }   
    

    //Enable/disable buttons for changing opinion
    var like_btn = $(total_span.parent().parent().find('#btnLike'));
    var dislike_btn = $(total_span.parent().parent().find('#btnDislike'));
    if (karma_data.opinion === 'like'){
	like_btn.attr('disabled','disabled');
	dislike_btn.removeAttr('disabled');
    } else if (karma_data.opinion === 'dislike') {
	like_btn.removeAttr('disabled');
	dislike_btn.attr('disabled','disabled');
    }
}
#!/usr/bin/env python
#Script to merge the JS files in use and publish them into one file

import os,sys

#Ensure the only required argument is there
if len(sys.argv) < 2:
    print("WARN: No version number provided, using default [dev]")
    version_num = 'dev'
else:
    version_num = sys.argv[1]


print("Merging active JS files for version (" + version_num + ")")

out_path = 'jsbundle-' + version_num + '.js'
out_fh = open(out_path,'wb')

JS_FILES = ['/home/vados/www/konvention/js/jquery/jquery-1.8.2.min.js',
            '/home/vados/www/konvention/plugins/bootstrap/js/bootstrap.min.js',
            '/home/vados/www/konvention/plugins/datatables/media/js/jquery.dataTables.min.js',
            '/home/vados/www/konvention/plugins/google-code-prettify/prettify.js',
            '/home/vados/www/konvention/plugins/jTruncate/jquery.jtruncate.min.js',
            '/home/vados/www/konvention/js/timeago/jquery.timeago.js',
            '/home/vados/www/konvention/js/modernizr.js',
            '/home/vados/www/konvention/js/main.js',
            ]

for path in JS_FILES:
    with open(path, 'rb') as fh:
        out_fh.write(fh.read())
        out_fh.write('\n')

#close fh        
out_fh.close()
print('Finished writing [' + out_path + ']')

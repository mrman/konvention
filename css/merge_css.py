#!/usr/bin/env python
#Script to merge the CSS files in use and publish them into one file

import os,sys

#Ensure the only required argument is there
if len(sys.argv) < 2:
    print("WARN: No version number provided, using default [dev]")
    version_num = 'dev'
else:
    version_num = sys.argv[1]


print("Merging active CSS files for version (" + version_num + ")")

out_path = 'cssbundle-' + version_num + '.css'
out_fh = open(out_path,'wb')

CSS_FILES = ['/home/vados/www/konvention/plugins/bootstrap/css/bootstrap.min.css',
            '/home/vados/www/konvention/plugins/datatables/media/css/demo_table.css',
            '/home/vados/www/konvention/plugins/google-code-prettify/prettify.css',
            '/home/vados/www/konvention/plugins/google-code-prettify/sons-of-obsidian.css',
            '/home/vados/www/konvention/plugins/webicons/fc-webicons.css',
            '/home/vados/www/konvention/css/main.css',
            '/home/vados/www/konvention/css/form.css',
            ]

for path in CSS_FILES:
    with open(path, 'rb') as fh:
        out_fh.write(fh.read())
        out_fh.write('\n')

#close fh        
out_fh.close()
print('Finished writing [' + out_path + ']')

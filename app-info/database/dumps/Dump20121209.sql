CREATE DATABASE  IF NOT EXISTS `vados_konvention` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `vados_konvention`;
-- MySQL dump 10.13  Distrib 5.5.28, for debian-linux-gnu (x86_64)
--
-- Host: 192.168.0.196    Database: vados_konvention
-- ------------------------------------------------------
-- Server version	5.5.28-1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Comment`
--

DROP TABLE IF EXISTS `Comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Comment` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(128) NOT NULL,
  `fk_type_id` int(11) NOT NULL,
  `username` varchar(128) NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `submitted_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment` text NOT NULL,
  PRIMARY KEY (`comment_id`),
  KEY `fk_index` (`fk_type_id`),
  KEY `username_index` (`username`),
  KEY `type_index` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `BestPracticeKarmaView`
--

DROP TABLE IF EXISTS `BestPracticeKarmaView`;
/*!50001 DROP VIEW IF EXISTS `BestPracticeKarmaView`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `BestPracticeKarmaView` (
  `bestpractice_id` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `description` tinyint NOT NULL,
  `ref_url` tinyint NOT NULL,
  `context_id` tinyint NOT NULL,
  `likes` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `BestPractice`
--

DROP TABLE IF EXISTS `BestPractice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BestPractice` (
  `bestpractice_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `ref_url` varchar(128) NOT NULL COMMENT 'This table holds the Best Practices for given contexts',
  `context_id` int(11) NOT NULL,
  `fk_karma_id` int(11) NOT NULL,
  PRIMARY KEY (`bestpractice_id`),
  KEY `context_id_idx` (`context_id`),
  CONSTRAINT `context_id` FOREIGN KEY (`context_id`) REFERENCES `Context` (`context_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `KarmaRecord`
--

DROP TABLE IF EXISTS `KarmaRecord`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `KarmaRecord` (
  `fk_type_id` int(11) NOT NULL,
  `type` varchar(128) NOT NULL,
  `user_id` varchar(45) NOT NULL,
  `opinion` enum('LIKE','DISLIKE') NOT NULL,
  PRIMARY KEY (`fk_type_id`),
  KEY `user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL COMMENT 'User table, contains user data.',
  PRIMARY KEY (`user_id`),
  KEY `username_index` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ActivityRecord`
--

DROP TABLE IF EXISTS `ActivityRecord`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ActivityRecord` (
  `username` varchar(128) NOT NULL,
  `desc` varchar(128) NOT NULL,
  `fk_type` varchar(128) NOT NULL,
  `fk_type_id` int(11) NOT NULL,
  `timeof` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `url` varchar(128) NOT NULL,
  `subject` varchar(128) NOT NULL,
  KEY `fk_user` (`username`),
  KEY `time_index` (`timeof`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UserInfo`
--

DROP TABLE IF EXISTS `UserInfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserInfo` (
  `user_id` int(11) NOT NULL,
  `facebook_url` varchar(128) DEFAULT NULL,
  `googleplus_url` varchar(128) DEFAULT NULL,
  `github_url` varchar(128) DEFAULT NULL,
  `twitter_url` varchar(128) DEFAULT NULL,
  `tumblr_url` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `fk_user` (`user_id`),
  CONSTRAINT `fk_user` FOREIGN KEY (`user_id`) REFERENCES `User` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Context`
--

DROP TABLE IF EXISTS `Context`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Context` (
  `context_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL COMMENT 'This table contains the contexts for various sub-classes (convetntions, best practices, etc)',
  `description` text NOT NULL,
  `ref_url` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`context_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ContextSuggestion`
--

DROP TABLE IF EXISTS `ContextSuggestion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ContextSuggestion` (
  `contextsuggestion_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `comment` text NOT NULL,
  `ref_url` varchar(128) NOT NULL,
  `fk_karma_id` int(11) NOT NULL,
  PRIMARY KEY (`contextsuggestion_id`),
  KEY `name_index` (`name`),
  KEY `karma` (`fk_karma_id`),
  CONSTRAINT `karma` FOREIGN KEY (`fk_karma_id`) REFERENCES `Karma` (`karma_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Karma`
--

DROP TABLE IF EXISTS `Karma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Karma` (
  `karma_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(128) NOT NULL,
  `fk_type_id` varchar(128) DEFAULT NULL,
  `likes` int(11) DEFAULT '0',
  `dislikes` int(11) DEFAULT '0',
  PRIMARY KEY (`karma_id`),
  KEY `fk_lookup_index` (`fk_type_id`),
  KEY `type_lookup_index` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Final view structure for view `BestPracticeKarmaView`
--

/*!50001 DROP TABLE IF EXISTS `BestPracticeKarmaView`*/;
/*!50001 DROP VIEW IF EXISTS `BestPracticeKarmaView`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`vados`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `BestPracticeKarmaView` AS select `bp`.`bestpractice_id` AS `bestpractice_id`,`bp`.`name` AS `name`,`bp`.`description` AS `description`,`bp`.`ref_url` AS `ref_url`,`bp`.`context_id` AS `context_id`,`k`.`likes` AS `likes` from (`BestPractice` `bp` join `Karma` `k`) where (`bp`.`fk_karma_id` = `k`.`karma_id`) order by `k`.`likes` desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Dumping events for database 'vados_konvention'
--

--
-- Dumping routines for database 'vados_konvention'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-12-09 19:17:46
